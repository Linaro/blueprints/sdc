all: inference-image-save

.PHONY: inference

i := inference
staging := out/staging/inference

inference: out/.stamp_inference

inference-image-push: FORCE out/.stamp_inference | $(builder-image)
	$(call run-builder,docker tag sc-inference $(DOCKER_NAMESPACE)/sc-inference)
	$(call run-builder,docker push $(DOCKER_NAMESPACE)/sc-inference)

images-push: inference-image-push

inference-image-save: out/sc-inference.tar.gz

out/.stamp_inference-staging: $(compiled-model) \
				$(i)/Dockerfile $(i)/inference.py \
				$(shell find $(i)/classes -type f) \
				$(shell find $(i)/config -type f) \
				$(shell find $(i)/src -type f)
	$(call mkdir,$(staging))
	$(call cp,$(i)/Dockerfile,$(staging))
	$(call cp,$(i)/inference.py,$(staging))
	$(call cp-r,$(i)/classes,$(staging))
	$(call cp-r,$(i)/config,$(staging))
	$(call cp-r,$(i)/src,$(staging))
	$(call mkdir,$(staging)/models/$(TRAINING_MODEL))
	$(call cp,out/$(TRAINING_MODEL)/models/*,$(staging)/models/$(TRAINING_MODEL))
	$(q)touch $@

out/.stamp_inference: out/.stamp_inference-staging | host-aarch64-registered $(builder-image)
	$(call run-builder,docker buildx build $(DOCKER_BUILD_OPTS) --platform aarch64 -t sc-inference $(staging))
	$(q)touch $@

out/sc-inference.tar.gz: out/.stamp_inference | $(builder-image)
	@$(quiet) "  DOCKSAV $@"
	$(q)$(call run-builder,bash -c 'docker save sc-inference | pigz >$@')

inference-builder-image: out/.stamp_inference-builder-image

out/.stamp_inference-builder-image: $(i)/builder/Dockerfile
	@$(quiet) "  DOCKBLD sc-inference-builder"
	$(q)docker build $(DOCKER_BUILD_OPTS) \
		--build-arg HOST_DOCKER_GID=$$(stat -c %g /var/run/docker.sock 2>/dev/null || echo 0) \
		--build-arg USER_ID=$$(id -u) \
		--build-arg USER_GID=$$(id -g) \
		-t sc-inference-builder $(i)/builder
	$(q)touch $@

staging-gg-inf = out/staging/greengrassv2/SmartCameraInference

greengrass-inference-component: $(staging-gg-inf)/zip-build/SmartCameraInference.zip

$(staging-gg-inf)/zip-build/SmartCameraInference.zip: $(staging-gg-inf)/sc-inference.tar.gz \
				$(staging-gg-inf)/gdk-config.json \
				$(staging-gg-inf)/recipe.yaml | $(builder-image)
	@$(quiet) "  BUILD   $@"
	$(q)$(call run-builder,bash -c 'cd $(staging-gg-inf) && gdk component build')

$(staging-gg-inf)/sc-inference.tar.gz: out/sc-inference.tar.gz
	$(call cp-file)

$(staging-gg-inf)/gdk-config.json: inference/greengrassv2/SmartCameraInference/gdk-config.json
	$(call cp-file)

$(staging-gg-inf)/recipe.yaml: inference/greengrassv2/SmartCameraInference/recipe.yaml
	$(call cp-file)

greengrass-inference-component-publish: $(staging-gg-inf)/zip-build/SmartCameraInference.zip \
				out/.stamp_greengrass-attach-role-policy | $(builder-image)
	@$(quiet) "  PUBLISH $(notdir $<)"
	$(q)$(call run-builder,./aws/gdk_component_publish.sh $(staging-gg-inf))

greengrass-components: greengrass-inference-component

greengrass-components-publish: greengrass-inference-component-publish
