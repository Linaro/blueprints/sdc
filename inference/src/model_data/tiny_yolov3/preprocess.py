"""
Copyright © 2024 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
import cv2
import numpy as np

class Preprocess:

    def __init__(self, acc='Ethos-U NPU'):
        self.img_size = (416, 416)

    def preprocess_image(self, img):

        """
            Preprocess a frame for object detection using a specified model.

            This function takes frame which is resized to a standard size and
            preprocesses it for object detection using the given model.

            Args:
                img (numpy.ndarray): The input video frame as a NumPy array.

            Returns:
                numpy.ndarray: The preprocessed image ready for object detection.
        """

        img_w, img_h = self.img_size
        height, width, _ = img.shape

        img_scale = min(img_w / width, img_h / height)
        new_w, new_h = int(img_scale * width), int(img_scale * height)
        img_resized = cv2.resize(img, (new_w, new_h))

        img_paded = np.full(shape=[img_h, img_w, 3], dtype=np.float32, fill_value=127)
        dw, dh = (img_w - new_w) // 2, (img_h - new_h) // 2
        img_paded[dh:new_h + dh, dw:new_w + dw, :] = img_resized

        img = img_paded[np.newaxis, ...]

        return img
