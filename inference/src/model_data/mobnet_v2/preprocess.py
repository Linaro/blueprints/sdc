"""
Copyright © 2024 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
import cv2
import numpy as np

class Preprocess:

    def __init__(self, acc):
        self.acc = acc
        pass

    def preprocess_image(self, img):
        """
            Preprocess a frame for classification using a specified model.

            This function takes frame which is resized to a standard size and
            preprocesses it for classification using the given model.

            Args:
                img (numpy.ndarray): The input video frame as a NumPy array.

            Returns:
                numpy.ndarray: The preprocessed image ready for classification.
        """

        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        img = img.reshape(1, img.shape[0], img.shape[1], img.shape[2])
        img = img.astype(np.int8)
        return img



