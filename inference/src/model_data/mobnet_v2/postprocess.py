"""
Copyright © 2024 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
import json
import numpy as np
import tensorflow as tf

class Postprocess:
    def __init__(self, score_threshold,class_ids):
        self.score_threshold = score_threshold
        self.model = "mobnet_v2"
        self.acc = "Cortex-A55 CPU"
        self.class_ids = class_ids
        # Load model details from JSON file
        with open("/config/model_detail.json", 'r') as file:
            self.model_details = json.load(file)
        self.top_classes_count = self.model_details.get(self.acc, {}).get(self.model, {}).get("top_classes_count")

    def softmax(self, x):
        """
        Compute softmax values for each sets of scores in x.

        Parameters:
        x : numpy array
            Input array

        Returns:
        numpy array
            Softmax of the input array
        """
        # Subtracting max value for numerical stability
        exp_x = np.exp(x - np.max(x))
        return exp_x / np.sum(exp_x)

    def postprocess_image(self,tfNet_lite,img):

        """
        Postprocesses classification results.

        This function takes the pre-processed input image and interpreter for running the model
        inference.

        Args:
            img (numpy.ndarray): Pre-processed Input image.
            tfNet_lite:  interpreter used for running the model inference.

        Returns:
            numpy.ndarray: Returns new_boxes, top_classes and confidence_score.
        """

        new_boxes = None
        output_details = tfNet_lite.get_output_details()
        pred = tfNet_lite.get_tensor(output_details[0]['index']).astype(np.float32)
        scores = tf.nn.softmax(pred[0])
        # scores_list = scores.numpy().tolist()
        top_classes = np.argsort(pred)[0, ::-1][:self.top_classes_count]
        confidence_score = [scores[i].numpy() *100 for i in top_classes]

        return new_boxes, top_classes, confidence_score
