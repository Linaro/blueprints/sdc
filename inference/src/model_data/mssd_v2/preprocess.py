"""
Copyright © 2024 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
import cv2, json
import numpy as np

class Preprocess:

    def __init__(self, acc):
        with open('config/model_detail.json','r') as m:
            model_info = json.load(m)
        self.model_dtype = model_info.get(acc, {}).get('mssd_v2',{}).get('model_dtype')
        self.acc = acc
        self.img_size = (300, 300)

    def preprocess_image(self, img):

        """
            Preprocess a frame for object detection using a specified model.

            This function takes frame which is resized to a standard size and
            preprocesses it for object detection using the given model.

            Args:
                img (numpy.ndarray): The input video frame as a NumPy array.

            Returns:
                numpy.ndarray: The preprocessed image ready for object detection.
        """

        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        img = cv2.resize(img, self.img_size)
        img = img.reshape(1, img.shape[0], img.shape[1], img.shape[2]) # (1, 300, 300, 3)
        if self.model_dtype == "Int8":
            img = img.astype(np.int8)
        elif self.model_dtype == "Uint8":
            img = img.astype(np.uint8)
        return img



