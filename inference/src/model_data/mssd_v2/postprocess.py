"""
Copyright © 2024 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

import numpy as np

class Postprocess:
    def __init__(self, score_threshold,class_ids):
        self.score_threshold = score_threshold
        self.class_ids = class_ids

    def postprocess_image(self, tfNet_lite, img):

        """
        Postprocesses object detection results.

        This function takes the pre-processed input image and interpreter for running the model
        inference.

        Args:
            img (numpy.ndarray): Pre-processed Input image.
            tfNet_lite:  interpreter used for running the model inference.

        Returns:
            numpy.ndarray: Returns right_boxes, right_classes and right_scores.
        """


        self.tfNet_lite = tfNet_lite
        self.output_details = tfNet_lite.get_output_details()
        self.b_boxes = tfNet_lite.get_tensor(self.output_details[0]['index'])
        self.label_index = tfNet_lite.get_tensor(self.output_details[1]['index'])
        self.scores = tfNet_lite.get_tensor(self.output_details[2]['index'])

        output_dict = {
        'detection_boxes': np.array(self.b_boxes),
        'detection_scores': np.array(self.scores),
        'detection_classes': np.array(self.label_index)
        }

        bboxes = output_dict['detection_boxes'][0]
        scores = output_dict['detection_scores'][0]
        classes = output_dict['detection_classes'][0]

        selected_indices = np.where(scores > self.score_threshold)[0]

        classes = classes[selected_indices]
        bboxes = bboxes[selected_indices]
        scores = scores[selected_indices]

        new_boxes, new_classes, new_scores = [], [], []
        for cls_id in range(len(bboxes)):
            if classes[cls_id] not in self.class_ids:
                continue
            new_boxes.append(bboxes[cls_id])
            new_classes.append(classes[cls_id])
            new_scores.append(scores[cls_id])

        right_boxes = np.array(new_boxes)
        right_classes = np.array(new_classes)
        right_scores = np.array(new_scores)


        return right_boxes, right_classes, right_scores