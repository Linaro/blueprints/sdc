"""
Copyright © 2024 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
import cv2, json
import numpy as np

class Preprocess:
    """
    Preprocess a video frame for object detection using a specified model.

    This function takes a video frame and preprocesses it for object detection using
    the given model. The frame is resized to a standard size, and based on the model
    additional preprocessing are applied.

    Args:
        frame (numpy.ndarray): The input video frame as a NumPy array.
        model (str): The name of the object detection model to be used.

    Returns:
        numpy.ndarray: The preprocessed image ready for object detection.
    """
    def __init__(self, acc):
        self.img_size = (300, 300)

    def preprocess_image(self, img):
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        img_resized = cv2.resize(img, (300, 300), interpolation=cv2.INTER_LINEAR)
        def normalize(img):
            img = np.asarray(img, dtype='float32')
            img /= 255.0
            img -= 0.5
            img *= 2.0
            return img
        img_normalized = normalize(img_resized)
        img_normalized = np.expand_dims(img_normalized, axis=0)
        return img_normalized




