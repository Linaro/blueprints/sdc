"""
Copyright © 2024 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
import cv2,random
import numpy as np
from ensemble_boxes import *
import math

class Postprocess:
    def __init__(self, score_threshold,class_ids):
        """
        Postprocesses object detection results.

        This function takes the pre-processed input image and interpreter for running the model
        inference.

        Args:
            img (numpy.ndarray): Pre-processed Input image.
            tfNet_lite:  interpreter used for running the model inference.

        Returns:
            numpy.ndarray: Returns right_boxes, right_classes and right_scores.
        """
        self.score_threshold = score_threshold
        self.class_ids = class_ids
        self.img_height = 300
        self.img_width = 300
        self.y_scale = 10.0
        self.x_scale = 10.0
        self.h_scale = 5.0
        self.w_scale = 5.0
        self.anchors = np.load('/src/model_data/mssd_v1/anchors/anchors.npy')
        
    def decode_box_encodings(self,box_encoding, anchors, num_boxes):
            decoded_boxes = np.zeros((num_boxes, 4), dtype=np.float32)
            for i in range(num_boxes):
                ycenter = box_encoding[i][0] / self.y_scale * self.anchors[i][2] + self.anchors[i][0]
                xcenter = box_encoding[i][1] / self.x_scale * self.anchors[i][3] + self.anchors[i][1]
                half_h = 0.5 * math.exp((box_encoding[i][2] / self.h_scale)) * self.anchors[i][2]
                half_w = 0.5 * math.exp((box_encoding[i][3] / self.w_scale)) * self.anchors[i][3]
                decoded_boxes[i][0] = (xcenter - half_w)  
                decoded_boxes[i][1] = (ycenter - half_h)  
                decoded_boxes[i][2] = (xcenter + half_w)  
                decoded_boxes[i][3] = (ycenter + half_h) 
            return decoded_boxes

    def postprocess_image(self, tfNet_lite, img):
        self.tfNet_lite = tfNet_lite
        self.output_details = tfNet_lite.get_output_details()
        out0, out1 = tfNet_lite.get_tensor(self.output_details[0]['index']), tfNet_lite.get_tensor(self.output_details[1]['index'])
        # out0 is class prediction , out1 is box encoding 
        labels = np.array([], dtype=int)
        result_labels = np.append(labels, np.argmax(out0[0], axis=1))
        indexes_not_background = np.where(result_labels != 0)
        labels_not_background = np.take(result_labels, indexes_not_background)
        scores = []
        scores.append(list(map(max, out0[0])))
        scores_not_background = np.take(scores[0], indexes_not_background)
        box_decoded = self.decode_box_encodings(out1[0], self.anchors, 1917)
        box_not_background = np.take(box_decoded, indexes_not_background, axis=0)
        box_clipped = box_not_background.clip(0, 1)
        if len(box_clipped[0].tolist()) > 0:
            right_bboxes, right_scores, right_classes = nms(box_clipped.tolist(), scores_not_background.tolist(), labels_not_background.tolist(), iou_thr=0.1)
            flat_right_boxes = np.array(right_bboxes).reshape(-1, 4)
            def swap_box_coordinates(boxes):
                swapped_boxes = []
                for box in boxes:
                    y0, x0, y1, x1 = box
                    swapped_boxes.append([x0, y0, x1, y1])
                return swapped_boxes
            right_boxes = swap_box_coordinates(flat_right_boxes)
        else:
            right_boxes = []
            right_classes = []
            right_scores = []
        right_boxes = np.array(right_boxes)
        right_classes = np.array(right_classes)
        right_scores = np.array(right_scores)
        return right_boxes, right_classes , right_scores 
