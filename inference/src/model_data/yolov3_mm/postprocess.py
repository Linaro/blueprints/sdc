"""
Copyright © 2024 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
import json
import numpy as np

class Postprocess:
    def __init__(self, score_threshold,class_ids):
        self.score_threshold = score_threshold
        self.class_ids = class_ids
        acc = 'Ethos-U NPU'
        model = 'yolov3_mm'
        with open('config/model_detail.json','r') as m:
            model_info = json.load(m)
        anchors_val = model_info.get(acc, {}).get(model, {}).get('anchors',{})
        mask_val = model_info.get(acc, {}).get(model, {}).get('mask',{})
        strides_val = model_info.get(acc, {}).get(model, {}).get('strides',{})

        self.anchors = np.array(list(map(lambda x: list(map(int, str.split(x, ','))), anchors_val.split())))
        self.mask = np.array(list(map(lambda x: list(map(int, str.split(x, ','))), mask_val.split())))
        self.strides = list(map(int, strides_val.split(',')))

        class_name_path = model_info.get(acc, {}).get(model, {}).get('class_name_path',{})
        self.num_classes = len(self.decode_class_names(class_name_path))

    def decode_class_names(self, classes_path):

        """
        Decode class names from a file.

        Args:
            classes_path (str): Path to the file containing class names, one per line.

        Returns:
            list: List of class names.
        """

        with open(classes_path, 'r') as f:
            lines = f.readlines()
        classes = []
        for line in lines:
            line = line.strip()
            if line:
                classes.append(line)
        return classes

    def sigmoid(self, x):

        """
        Compute the sigmoid function.

        Args:
            x (numpy.ndarray): Input array.

        Returns:
            numpy.ndarray: Output array after applying the sigmoid function element-wise.
        """

        return (1 / (1 + np.exp(-x)))

    def post_process_tiny_predictions(self, inputs, ind):

        """
        Post-processes model predictions for object detection

        Args:
            inputs (numpy.ndarray): Model prediction inputs.
            ind (int): Index of the desired anchors subset.


        Returns:
            tuple: A tuple containing arrays of all_boxes, all_scores, and all_classes.
                - all_boxes (numpy.ndarray): Bounding boxes of detected objects.
                - all_scores (numpy.ndarray): Confidence scores of detected objects.
                - all_classes (numpy.ndarray): Class labels of detected objects.
        """

        logits = inputs[0]
        i = ind
        anchors, stride = self.anchors[self.mask[i]], self.strides[i]
        x_shape = np.shape(logits)
        logits = np.reshape(logits, (x_shape[0], x_shape[1], x_shape[2], len(anchors), self.num_classes + 5))

        box_xy, box_wh, obj, cls = logits[...,:2], logits[...,2:4], logits[...,4], logits[...,5:]
        box_xy = self.sigmoid(box_xy)
        obj = self.sigmoid(obj)
        cls = self.sigmoid(cls)

        grid_shape = x_shape[1:3]
        grid_h, grid_w = grid_shape[0], grid_shape[1]
        anchors = np.array(anchors, dtype=np.float32)
        grid = np.meshgrid(np.arange(grid_w), np.arange(grid_h))
        grid = np.expand_dims(np.stack(grid, axis=-1), axis=2)  # [gx, gy, 1, 2]

        box_xy = (box_xy + np.array(grid, dtype=np.float32)) * stride
        box_wh = np.exp(box_wh) * np.array(anchors, dtype=np.float32)

        box_x1y1 = box_xy - box_wh / 2.
        box_x2y2 = box_xy + box_wh / 2.
        box = np.concatenate([box_x1y1, box_x2y2], axis=-1)

        all_boxes = np.reshape(box, (x_shape[0], -1, 1, 4))
        objects = np.reshape(obj, (x_shape[0], -1, 1))
        all_classes = np.reshape(cls, (x_shape[0], -1, self.num_classes))

        all_scores = objects * all_classes
        return all_boxes, all_scores, all_classes

    def _nms_boxes(self, boxes, scores, iou_thresh=0.45):

        """
        Apply non-maximum suppression to filter out overlapping boxes.

        Args:
            boxes (numpy.ndarray): Bounding boxes to be filtered.
            scores (numpy.ndarray): Confidence scores corresponding to the boxes.
            iou_thresh (float, optional): Intersection over Union threshold for suppression.
                                        Default is 0.45.

        Returns:
            keep (numpy.ndarray): A list containing arrays of nms_boxes.
        """

        x = boxes[:, 0]
        y = boxes[:, 1]
        w = boxes[:, 2] -  boxes[:, 0]
        h = boxes[:, 3] -  boxes[:, 1]

        areas = (w)* (h)
        order = scores.argsort()[::-1]

        keep = []
        while order.size > 0:
            i = order[0]
            keep.append(i)

            xx1 = np.maximum(x[i], x[order[1:]])
            yy1 = np.maximum(y[i], y[order[1:]])
            xx2 = np.minimum(x[i] + w[i], x[order[1:]] + w[order[1:]])
            yy2 = np.minimum(y[i] + h[i], y[order[1:]] + h[order[1:]])

            w1 = np.maximum(0.0, xx2 - xx1 + 1)
            h1 = np.maximum(0.0, yy2 - yy1 + 1)
            inter = w1 * h1

            ovr = inter / (areas[i] + areas[order[1:]] - inter)
            inds = np.where(ovr <= iou_thresh)[0]
            order = order[inds + 1]

        keep = np.array(keep)
        return keep

    def filter_boxes_(self, all_boxes, all_scores):

        """
        Filter boxes based on confidence scores & Non maximum suppression.

        Args:
            all_boxes (numpy.ndarray): Bounding boxes to be filtered.
            all_scores (numpy.ndarray): Confidence scores corresponding to the boxes.


        Returns:
            tuple: A tuple containing arrays of filtered_boxes, filtered_classes, and filtered_scores.
                - filtered_boxes (numpy.ndarray): Bounding boxes after filtering.
                - filtered_classes (numpy.ndarray): Class labels corresponding to the filtered boxes.
                - filtered_scores (numpy.ndarray): Confidence scores after filtering.
        """

        box_classes = np.argmax(all_scores, axis=-1)
        box_class_scores = np.max(all_scores, axis=-1)
        pos = np.where(box_class_scores >= self.score_threshold)

        fil_boxes = all_boxes[pos]
        fil_boxes = np.reshape(fil_boxes, (fil_boxes.shape[0], fil_boxes.shape[2]))
        fil_classes = box_classes[pos]
        fil_scores = box_class_scores[pos]

        nboxes, nclasses, nscores = [], [], []
        for c in set(fil_classes):
            inds = np.where(fil_classes == c)
            b = fil_boxes[inds]
            c = fil_classes[inds]
            s = fil_scores[inds]

            keep = self._nms_boxes(b, s)

            nboxes.append(b[keep])
            nclasses.append(c[keep])
            nscores.append(s[keep])

        if not nclasses and not nscores:
            return None, None, None

        return np.concatenate(nboxes), np.concatenate(nclasses), np.concatenate(nscores)

    def postprocess_image(self, tfNet_lite, img):

        """
        Postprocesses object detection results.

        This function takes the pre-processed input image and interpreter for running the model
        inference.

        Args:
            img (numpy.ndarray): Pre-processed Input image.
            tfNet_lite:  interpreter used for running the model inference.

        Returns:
            numpy.ndarray: Returns right_boxes, right_classes and right_scores.
        """

        output_details = tfNet_lite.get_output_details()
        out0, out1 = tfNet_lite.get_tensor(output_details[0]['index']), tfNet_lite.get_tensor(output_details[1]['index'])
        big_all_boxes, big_all_scores, big_all_classes = self.post_process_tiny_predictions([out0], 0)
        small_all_boxes, small_all_scores, small_all_classes = self.post_process_tiny_predictions([out1], 1)

        bboxes = np.concatenate([big_all_boxes, small_all_boxes], axis=1)
        scores = np.concatenate([big_all_scores, small_all_scores], axis=1)
        classes = np.concatenate([big_all_classes, small_all_classes], axis=1)

        right_boxes, right_classes, right_scores = self.filter_boxes_(bboxes, scores)

        new_boxes, new_classes, new_scores = [], [], []
        if right_classes is not None:
            if right_boxes is not None:
                for cls_id in range(len(right_boxes)):
                    if right_classes[cls_id] not in self.class_ids:
                        continue

                    bbox = right_boxes[cls_id]

                    xmin = (bbox[1]/img.shape[1])
                    ymin = (bbox[0]/img.shape[0])
                    xmax = (bbox[3]/img.shape[1])
                    ymax = (bbox[2]/img.shape[0])

                    new_boxes.append([xmin, ymin, xmax, ymax])
                    new_classes.append(right_classes[cls_id])
                    new_scores.append(right_scores[cls_id])

        right_boxes = np.array(new_boxes)
        right_classes = np.array(new_classes)
        right_scores = np.array(new_scores)

        return right_boxes, right_classes, right_scores