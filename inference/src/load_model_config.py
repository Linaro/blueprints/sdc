"""
Copyright © 2024 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
import json
import os
import tflite_runtime.interpreter as tflite
from src.config import Config
from src.logger import get_logger
from src.custom_exception import ModelNotFoundError

logger = get_logger()

def load_model(acc, model, model_path, ext_delegate):
    """
    Load a TFlite compiled model from a specified path.

    This function loads a default yolov3 model from the given file path.

    Args:
        model_path : The path to the model file to be loaded.
        model : Provided model name to be loaded
        acc : Accelerator to be used for inferencing

    Returns:
        model: The loaded machine learning model.

    Raises:
        ModelNotFoundError: Raises custom exception, If the specified model file is not found at the given path.
    """
    logger.info("model_name:%s ", model)
    logger.info("model_path:%s", model_path)

    if acc == "npu":
        ext_delegate_options = {}
        logger.info('Loading external delegate from {} with args: {}'.format(ext_delegate, ext_delegate_options))
        ext_delegate = [tflite.load_delegate(ext_delegate, ext_delegate_options)]

    elif acc == "cpu":
        ext_delegate = None

    if os.path.exists(model_path):
        interpreter = tflite.Interpreter(model_path=model_path, experimental_delegates=ext_delegate)
        interpreter.allocate_tensors()
        logger.info("Model loaded successfully.")
        return interpreter
    else:
        raise ModelNotFoundError("Model not found.")

def load_model_config(model, acc, conf_file):
    """
    Load and apply configuration settings for a model.

    This function loads configuration settings from a JSON file and applies them to the provided model.

    Args:
        model (Model): The model to which the configuration settings will be applied.
        conf_file (str, optional): The path to the JSON configuration file. Default is "config/yolov3.json".

    Returns:
        config: Return instance of config class with loaded configuration.

    Raises:
        FileNotFoundError: If the specified configuration file is not found.
        JSONDecodeError: If there is an error decoding the JSON content from the configuration file.
    """
    if os.path.exists(conf_file):
        config_json = json.load(open(conf_file))
        logger.info("Loaded model config for %s.", model)
    else:
        raise FileNotFoundError("No such a file or directory, unable to load the config file for model.")

    config = Config()
    if acc == "cpu":
        acc_="Cortex-A55 CPU"

    elif acc == "npu":
        acc_="Ethos-U NPU"

    config.model_path = config_json.get(acc_, {}).get(model, {}).get("model_path")
    config.desired_class_ids = config_json.get(acc_, {}).get(model, {}).get("desired_class_ids")
    config.score_threshold = config_json.get(acc_, {}).get(model, {}).get('score_threshold')

    return config