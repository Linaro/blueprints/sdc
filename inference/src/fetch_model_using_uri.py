"""
Copyright © 2024 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
import boto3
import os
import json
from src.logger import get_logger
from botocore.exceptions import NoCredentialsError, PartialCredentialsError, ClientError

logger = get_logger()

s3 = boto3.client('s3')

with open("config/aws_credential.json",'r') as config_file:
    aws_data = json.load(config_file)
    if aws_data is None:
        logger.warning("AWS config file is not available or invalid.")
        raise ValueError("AWS config file is not available or invalid.")
    logger.info("Credentials loaded from AWS config file")


def download_custom_s3(s3_uri, local_dir):
    """
    Download model folder from s3 bucket.

    This function downloads the custom model folder from AWS s3 bucket into local directory on device to
    provide custom model support in SDC.

    Args:
        s3_uri : Link to folder in the s3 bucket.
        local_dir : Path to which files to be downloaded from s3 bucket.

    """

    s3 = boto3.client('s3',
                aws_access_key_id=aws_data["accessKey"],
                aws_secret_access_key=aws_data["secretKey"],
                region_name=aws_data["region"]
                )

    if not s3_uri.startswith('s3://'):
        raise ValueError('Invalid S3 URI. It should start with "s3://".')

    s3_uri = s3_uri[len('s3://'):].split('/', 1)
    bucket_name = s3_uri[0]
    s3_prefix = s3_uri[1] if len(s3_uri) > 1 else ''

    if not os.path.exists(local_dir):
        os.makedirs(local_dir)

    paginator = s3.get_paginator('list_objects_v2')
    iterator = paginator.paginate(Bucket=bucket_name, Prefix=s3_prefix)

    for page in iterator:
        if 'Contents' in page:
            for obj in page['Contents']:
                s3_key = obj['Key']
                relative_path = s3_key[len(s3_prefix):].lstrip('/')

                local_filepath = os.path.join(local_dir, relative_path)

                local_file_dir = os.path.dirname(local_filepath)
                if not os.path.exists(local_file_dir):
                    os.makedirs(local_file_dir)

                s3.download_file(bucket_name, s3_key, local_filepath)

def download_model_s3(s3_uri, local_dir, file_to_check):
    """
    Download model folder from s3 bucket.

    This function downloads the custom model folder from AWS s3 bucket into local directory on device to
    provide custom model support in SDC.

    Args:
        s3_uri : Link to folder in the s3 bucket.
        local_dir : Path to which files to be downloaded from s3 bucket.
        file_to_check : File name to download from s3 bucket.

    """

    s3 = boto3.client('s3',
                aws_access_key_id=aws_data["accessKey"],
                aws_secret_access_key=aws_data["secretKey"],
                region_name=aws_data["region"]
                )

    if not s3_uri.startswith('s3://'):
        raise ValueError('Invalid S3 URI. It should start with "s3://".')

    s3_uri = s3_uri[len('s3://'):].split('/', 1)
    bucket_name = s3_uri[0]
    s3_key = s3_uri[1]

    object_key = os.path.join(s3_key, file_to_check)

    if not os.path.exists(local_dir):
        os.makedirs(local_dir)

    try:

        s3.head_object(Bucket=bucket_name, Key=object_key)
        print(f"Object {object_key} exists. Proceeding to download.")

        if not os.path.exists(local_dir):
            os.makedirs(local_dir)
        elif not os.path.isdir(local_dir):
            raise NotADirectoryError(f"Expected '{local_dir}' to be a directory, but it is a file.")

        local_filepath = os.path.join(local_dir, os.path.basename(object_key))

        s3.download_file(bucket_name, object_key, local_filepath)
        print(f"File {object_key} downloaded successfully to {local_filepath}")

    except ClientError as e:
        if e.response['Error']['Code'] == '404':
            print(f"Error: The specified object '{object_key}' does not exist in the bucket.")
        else:
            print(f"Error: {e}")
    except NoCredentialsError:
        print("Error: No AWS credentials found.")
    except PartialCredentialsError:
        print("Error: Incomplete AWS credentials.")
    except Exception as e:
        print(f"Unexpected error: {e}")

