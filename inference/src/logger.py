"""
Copyright © 2024 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
import logging
import logging.config
import boto3
import botocore
import warnings
import urllib3
from boto3.compat import PythonDeprecationWarning

def get_logger(file_path="/config/logger.conf"):
    """
    Set up and configure a logger based on the provided file path.

    Args:
        file_path (str): Path to the logger configuration file.

    Returns:
        logger: Configured logger instance.
    """
    warnings.filterwarnings("ignore", category=PythonDeprecationWarning)
    logging.getLogger('urllib3').setLevel(logging.WARNING)
    logging.getLogger('botocore').setLevel(logging.WARNING)
    logging.getLogger('boto3').setLevel(logging.WARNING)
    logging.getLogger('numba.core').setLevel(logging.WARNING)
    logging.config.fileConfig(file_path)
    # creating logger
    logger = logging.getLogger('Inference')
    return logger

