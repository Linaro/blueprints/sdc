"""
Copyright © 2024 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

from grpc.tools import protoc

protoc.main(
	(
		'',
		'-I.',
		'--python_out=.',
		'--grpc_python_out=.',
		'./Datas.proto'
	)
)
