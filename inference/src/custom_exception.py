"""
Copyright © 2024 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

class ModelLoadError(Exception):
    """
    Exception raised when there is an error loading a model.
    """
    pass

class ModelNotFoundError(Exception):
    """
    Exception raised when a specified model is not found.
    """
    pass
