"""
Copyright © 2024 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
import argparse

def ArgConf():
    """
    Parse command line arguments.

    Returns:
        dict: A dictionary containing parsed command line arguments.
              Keys: "port"
    """
    parser = argparse.ArgumentParser(description="Parse command line arguments.")
    parser.add_argument("-p", "--port", required=True, help="Port number (not 5000)")
    parser.add_argument("-o",'--ext_delegate_options',help='external delegate options')
    parser.add_argument("-m", "--model", required=True, help="Model to be used (tiny_yolov3/yolov3/mssd_v1/mssd_v2/mobilenetv2)")
    parser.add_argument("-a", "--accelerator", required=True, default="cpu", help="Accelerator to be used (cpu/npu)")
    parser.add_argument("-uri","--uri",help="link to download the model provided by the user")
    parser.add_argument("-uc", "--usecase", help="usecases to be used(tm/mm)")

    args = vars(parser.parse_args())

    return args
