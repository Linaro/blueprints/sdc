"""
Copyright © 2024 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
"""
utils.py:
    Preprocess the image for passing it to the inference for the specific model passed as an argument.
"""
import numpy as np
from src.logger import get_logger
from src.custom_exception import ModelNotFoundError

def preprocessing_img(img, acc, model):
    """
    Preprocess an image for a specified model.

    Args:
        img (numpy.ndarray): The input image as a numpy array.
        acc (str) :The accelerator name (cpu/npu)
        model (str): The model name, either "yolov3" or "tiny_yolov3".

    Returns:
        numpy.ndarray: The preprocessed image as a numpy array.

    Raises:
        ModelNotFoundError: If the given model is not supported.
    """
    logger = get_logger()

    if model in ["yolov3", "tiny_yolov3"]:
        img = np.reshape(img, (1, 416, 416, 3))
        img = img.astype(np.float32)
        img = img / 255.

    elif model == "mssd_v1":
        img = np.reshape(img, (1, 300, 300, 3))
        if acc == "cpu":
            img = img.astype(np.uint8)
        else:
            img = img.astype(np.int8)

    elif model == "mssd_v2":
        img = np.reshape(img, (1, 300, 300, 3))
        img = img.astype(np.uint8)

    elif model == "mobnet_v2":
        img = img.astype(np.int8)
        img = np.reshape(img, (1, 224, 224, 3))

    else:
        logger.error("Preprocessing model %s is not supported.", model)
        raise ModelNotFoundError("Given Model not found.")

    return img

def decode_class_names(classes_path):
    """
    Decode class names from a text file.

    This function reads class names from a text file, one per line, and returns them as a list.

    Args:
        classes_path (str): The path to the text file containing class names.

    Returns:
        list: A list of class names.

    """

    with open(classes_path, 'r') as file:
        lines = file.readlines()

    classes = []
    for line in lines:
        line = line.strip()
        if line:
            classes.append(line)

    return classes
