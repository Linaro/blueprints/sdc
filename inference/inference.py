"""
Copyright © 2024 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

"""
inference.py:
    Gathers the image data from the application container using gRPC service "MainServer" for channel "getStream".
    Reply back the predicted result as an response to the image data request."
"""

import json
import time
import cv2
import grpc
import numpy as np
from concurrent import futures
from src.argconfig import ArgConf
from src.grpc import Datas_pb2
from src.grpc import Datas_pb2_grpc
from src.load_model_config import load_model_config, load_model
from src.custom_exception import ModelLoadError, ModelNotFoundError
from src.logger import get_logger
from src.fetch_model_using_uri import download_model_s3,download_custom_s3

global score_threshold, model, class_name_path,logger
global model_name, acc

model_path=""
class_path=""
s3_uri=""
VALID_MODELS = ["yolov3", "tiny_yolov3", "mssd_v1", "mssd_v2", "mobnet_v2", "yolov3_mm"]

#logger instance
logger = get_logger()

#extract arguments
args = ArgConf()
model_name = args["model"]
acc = args["accelerator"]
delegate_path = args["ext_delegate_options"]
usecase = args["usecase"]

#assign pre/post process files w.r.t. models
if usecase == "tm" :
    if model_name == 'mssd_v1':
        from src.model_data.mssd_v1.preprocess import Preprocess
        from src.model_data.mssd_v1.postprocess import Postprocess

    elif model_name == "mssd_v2":
        from src.model_data.mssd_v2.preprocess import Preprocess
        from src.model_data.mssd_v2.postprocess import Postprocess

    elif model_name == 'tiny_yolov3':
        from src.model_data.tiny_yolov3.preprocess import Preprocess
        from src.model_data.tiny_yolov3.postprocess import Postprocess

    elif model_name == 'yolov3':
        from src.model_data.yolov3.preprocess import Preprocess
        from src.model_data.yolov3.postprocess import Postprocess

    elif model_name == 'mobnet_v2':
        from src.model_data.mobnet_v2.preprocess import Preprocess
        from src.model_data.mobnet_v2.postprocess import Postprocess

elif usecase == "mm":
    if model_name == 'yolov3_mm':
        from src.model_data.yolov3_mm.preprocess import Preprocess
        from src.model_data.yolov3_mm.postprocess import Postprocess


logger.info("Inference running on - %s",acc)

def validate_args(args):
    """
    Validates the argument passed from command-line & raise the exception for non valid argument.
    Also download the model from S3 if uri is provided.

    Args:
        None
    Returns:
        None
    """
#downloading model & files from S3 w.r.t. models
    if args["model"] in VALID_MODELS:
        if args["uri"] is not None:
            download_path = f'./custom_model/'

            logger.info("Model name is from valid models list. Fetching new model from S3 Bucket using URI - %s", args["uri"])
            if args["accelerator"] == "cpu":
                model=f"{args['model']}_cpu.tflite"
            else:
                model=f"{args['model']}_npu.tflite"
            download_model_s3(args["uri"],download_path,model)
            logger.info("Download complete for S3 folder")

    elif args["model"] == "custom":
        if args["uri"] == "local":
            pass
        else:
            download_path= f'./custom_model/'

            logger.info("Custom model selected. Fetching model folder using URI...")
            logger.info("Model download path: %s",download_path)
            download_custom_s3(args["uri"],download_path)


    if int(args['port']) == 5000 or int(args['port']) >=0 and int(args['port']) < 1024:
        logger.warning("Please use ports other than 0 to 1024 & 5000.")

def get_args():
    """
    Load the configuration file passed as an parameter & returns the argument.

    Args:
        None

    Returns:
        args: Argument's dictionary object with required model configuration.
    """
    args = ArgConf()
    return args

class Inference(Datas_pb2_grpc.MainServerServicer):
    def __init__(self, args, debug=0):
        """
        Inference class constructer, validates & loads the configuration for model & load the model.

        Args:
            args: Argument passed from command-prompt.
            debug: Enable debugging to print debugging logs.

        Returns:
            None
        """
        global preprocess_obj, postprocess_obj, Preprocess, Postprocess

        # Load model configuration
        validate_args(args)
        self.debug = debug
        self.args = args

        if args["model"] in VALID_MODELS:
            self.config_path = "config/model_detail.json"
            if debug:
                logger.debug("Loading model configuration %s.", model_name)
            self.config = load_model_config(model_name, acc, self.config_path)
            if debug:
                logger.debug("Model Configuration loaded.")
                logger.debug("Loading %s Model.", model_name)
            if args["uri"] is not None:
                if args["accelerator"] == "cpu":
                    self.config.model_path = f'custom_model/{model_name}_cpu.tflite'
                else:
                    self.config.model_path = f'custom_model/{model_name}_npu.tflite'
                logger.info("------LOADING CUSTOM MODEL TAKEN FROM S3 BUCKET---------")
        elif args["model"] == "custom":

            if args["uri"] is not None:
                self.config_path = f'custom_model/model_detail.json'
                #load model config
                if debug:
                    logger.debug("Loading model configuration %s.", model_name)
                self.config = load_model_config(model_name, acc, self.config_path)
                if debug:
                    logger.debug("Model Configuration loaded.")
                    logger.debug("Loading %s Model.", model_name)
                if args["accelerator"] == "cpu":
                    self.config.model_path = f'custom_model/{model_name}_cpu.tflite'
                else:
                    self.config.model_path = f'custom_model/{model_name}_npu.tflite'
                from custom_model.preprocess import Preprocess
                from custom_model.postprocess import Postprocess




        # load the model.
        start = time.time()
        self.model = load_model(acc, model_name, self.config.model_path, delegate_path)
        end = time.time()
        if self.model is None:
            raise ModelLoadError("Something went wrong, unable to load model.")
        self.model_load_time = (end-start)
        if debug:
            logger.debug("Model Loaded successfully, Time Taken to load: %s.", str(self.model_load_time))
        logger.info("Inferencing Module initialized successfully!")

        #initalizing pre/post process
        preprocess_obj = Preprocess(acc)
        postprocess_obj = Postprocess(self.config.score_threshold, self.config.desired_class_ids)

        right_boxes=0

    def getStream(self, request_iterator, context):
        """
        Get the set of requests received in iterator & extract the camera frame, process it and send for prediction.
        Also postprocess the output results & reply it back using gRPC to the requestor.

        Args:
            request_iterator: Iterator for received requests.
            context: NA

        Yields:
            Datas_pb2.Reply(reply=msg): Yields the msg.
            """
        #get model input, output details
        input_details = self.model.get_input_details()

        for req in request_iterator:
            #loading & decoding frames
            start = time.time()
            if self.debug:
                logger.debug("Loading Frame buffer")
            dBuf = np.frombuffer(req.datas, dtype=np.uint8)
            if dBuf is None:
                logger.error("Failed while loading buffer.")
                continue
            if self.debug:
                logger.debug("Decoding the jpeg buffer data to numpy array.")
            img = cv2.imdecode(dBuf, cv2.IMREAD_COLOR)
            if img is None:
                logger.error("Failed while decoding the image.")
                continue

            #preprocessing
            prepro_start_time = time.time()
            processed_img = preprocess_obj.preprocess_image(img)
            preprocess_time = time.time() - prepro_start_time

            self.model.set_tensor(input_details[0]['index'], processed_img)

            #inferencing
            start_run = time.time()
            self.model.invoke()
            end_run = time.time()

            inference_time = 1/(end_run - start_run)


            #postprocessing
            postpro_start = time.time()
            right_boxes, right_classes, right_scores = postprocess_obj.postprocess_image(self.model, img)
            postprocess_time = time.time() - postpro_start

            logger.info("Inference-side Preprocessing Time: %s", round(preprocess_time,4))
            logger.info("Pure Inference Time: %s", str(end_run - start_run))
            logger.info("Inference-side Postprocessing Time: %s", round(postprocess_time,4))

            #create json for GRPC reply
            if args["model"] == "mobnet_v2":
                if right_classes is None:
                    msg = json.dumps({"right_boxes":0, "right_scores":0, "right_classes":0,
                                      "model_load":str(self.model_load_time),
                                      "inf_time":str(inference_time)})
                else:
                    msg = json.dumps({"right_boxes":0,
                                      "right_scores":right_scores,
                                      "right_classes":right_classes.tolist(),
                                      "model_load":str(self.model_load_time),
                                      "inf_time":str(inference_time)})
            else:
                if right_boxes is None:
                    msg = json.dumps({"right_boxes":0, "right_classes":0,
                                    "right_scores":0, "model_load":str(self.model_load_time),
                                    "inf_time":str(inference_time)})
                else:
                    msg = json.dumps({"right_boxes":right_boxes.tolist(),
                                    "right_classes":right_classes.tolist(),
                                    "right_scores":right_scores.tolist(),
                                    "model_load":str(self.model_load_time),
                                    "inf_time":str(inference_time)})
            end = time.time()
            logger.info("Total Inference_time: %s.", str(end - start))

            #Sending data back to application cont through GRPC
            yield Datas_pb2.Reply(reply=msg)


def serve(args):
    """
    Start and serve a gRPC server for Infernce worker threads.

    Args:
        args (dict): A dictionary containing model configuration arguments passed from command-line.
                     Should at least contain "port, model & cpu_cores".

    Returns:
        None
    """
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    Datas_pb2_grpc.add_MainServerServicer_to_server(Inference(args), server)

    server_address = '[::]:' + str(args["port"])
    server.add_insecure_port(server_address)
    server.start()

    logger.info('GRPC server started')
    logger.info("GRPC Port number : %s.", str(args["port"]))

    server.wait_for_termination()

if __name__ == '__main__':
    args = get_args()
    serve(args)


