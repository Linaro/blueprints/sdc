# Builds a container image to be used on the target device to run some tests
# that require packages that may be missing from the host (such as the AWS CLI)

all: test-image-save

.PHONY: test

test: out/.stamp_test

test-image-push: FORCE out/.stamp_test | $(builder-image)
	$(call run-builder,docker tag sc-test $(DOCKER_NAMESPACE)/sc-test)
	$(call run-builder,docker push $(DOCKER_NAMESPACE)/sc-test)

images-push: test-image-push

test-image-save: out/sc-test.tar.gz

out/.stamp_test: test/Dockerfile | host-aarch64-registered $(builder-image)
	$(call run-builder,docker buildx build $(DOCKER_BUILD_OPTS) --platform aarch64 \
		--build-arg AWS_REGION=$(AWS_REGION) \
		--build-arg AWS_ACCESS_KEY_ID=$(AWS_ACCESS_KEY_ID) \
		--build-arg AWS_SECRET_ACCESS_KEY=$(AWS_SECRET_ACCESS_KEY) \
		-t sc-test test)
	$(q)touch $@

out/sc-test.tar.gz: out/.stamp_test | $(builder-image)
	@$(quiet) "  DOCKSAV $@"
	$(q)$(call run-builder,bash -c 'docker save sc-test | pigz >$@')

staging-gg-test := out/staging/greengrassv2/SmartCameraTest

greengrass-test-component: $(staging-gg-test)/zip-build/SmartCameraTest.zip

$(staging-gg-test)/zip-build/SmartCameraTest.zip: $(staging-gg-test)/sc-test.tar.gz \
				$(staging-gg-test)/gdk-config.json \
				$(staging-gg-test)/recipe.yaml | $(builder-image)
	@$(quiet) "  BUILD   $@"
	$(q)$(call run-builder,bash -c 'cd $(staging-gg-test) && gdk component build')

$(staging-gg-test)/sc-test.tar.gz: out/sc-test.tar.gz
	$(call cp-file)

gg-cw := test/greengrassv2/SmartCameraTest

$(staging-gg-test)/gdk-config.json: $(gg-cw)/gdk-config.json
	$(call cp-file)

$(staging-gg-test)/recipe.yaml: $(gg-cw)/recipe.yaml
	$(call cp-file)

greengrass-test-component-publish: $(staging-gg-test)/zip-build/SmartCameraTest.zip \
				out/.stamp_greengrass-attach-role-policy | $(builder-image)
	@$(quiet) "  PUBLISH $(notdir $<)"
	$(q)$(call run-builder,./aws/gdk_component_publish.sh $(staging-gg-test))

greengrass-components-publish: greengrass-test-component-publish
