# make imx93-bsp-image[-save]
#
# Build a Docker image (sc-imx83-bsp) containing the root FS of the NXP BSP 
# The image can later be used to run the TensorFlow example that verifies
# that the Ethos U-65 acceleration works:
#  docker run -it --tm sc-imx93-bsp
#  cd /usr/bin/tensorflow-lite-2.9.1/examples
#  python3 ./label_image.py  # CPU only
#  vela mobilenet_v1_1.0_224_quant.tflite
#  python3 ./label_image.py -m output/mobilenet_v1_1.0_224_quant_vela.tflite  # With NPU

imx93-bsp-zip := LF_v6.1.36-2.1.0_images_IMX93EVK.zip

out/imx93/$(imx93-bsp-zip):
	@echo
	@echo "Unfortunately the i.MX93 BSP cannot be downloaded automatically from the NXP site"
	@echo "due to a click-through licence."
	@echo "Please go to:"
	@echo "  https://www.nxp.com/design/software/embedded-software/i-mx-software/embedded-linux-for-i-mx-applications-processors:IMXLINUX"
	@echo "and download it manually."
	@echo "Save the file as: $@"
	$(q)false

out/imx93/nand: out/imx93/$(imx93-bsp-zip) | $(builder-image)
	$(call run-builder,bash -c 'cd out/imx93 && unzip $(imx93-bsp-zip) imx-image-full-imx93evk.wic && mv imx-image-full-imx93evk.wic nand')
	$(q)touch $@

out/.stamp_imx93-bsp-image: out/imx93/nand | $(builder-image)
	$(call run-builder,imx93/make_docker_img_from_dsk_img.sh 2 $<,sc-builder,-u 0:0)
	$(q)touch $@

out/sc-imx93-bsp.tar.gz: out/.stamp_imx93-bsp-image | $(builder-image)
	@$(quiet) "  DOCKSAV $@"
	$(q)$(call run-builder,bash -c 'docker save sc-imx93-bsp | pigz >$@')

imx93-bsp-image: out/.stamp_imx93-bsp-image

imx93-bsp-image-save: out/sc-imx93-bsp.tar.gz
