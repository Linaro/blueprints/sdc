#!/usr/bin/env python3

import fcntl
import os
import struct
import sys


_IOC_NRBITS = 8
_IOC_TYPEBITS = 8
_IOC_SIZEBITS = 14
_IOC_DIRBITS = 2

_IOC_NRSHIFT = 0
_IOC_TYPESHIFT = _IOC_NRSHIFT + _IOC_NRBITS
_IOC_SIZESHIFT = _IOC_TYPESHIFT + _IOC_TYPEBITS
_IOC_DIRSHIFT = _IOC_SIZESHIFT + _IOC_SIZEBITS

_IOC_NONE = 0
_IOC_WRITE = 1
_IOC_READ = 2


def _IOC(dir, type, nr, size):
  return (dir << _IOC_DIRSHIFT) | (type << _IOC_TYPESHIFT) | (nr << _IOC_NRSHIFT) | (size << _IOC_SIZESHIFT)


buf = bytearray(0x34)
req = _IOC(_IOC_READ, 0x1, 0x2, len(buf))
dev = "/dev/ethosu0"

print(f"Opening {dev}...")
fd = os.open(dev, os.O_RDWR | os.O_NONBLOCK)

print("Requesting device capabilities...")
fcntl.ioctl(fd, req, buf)

# struct ethosu_uapi_device_capabilities
rsp = (
  # struct ethosu_uapi_device_hw_id
  hw_id_version_status,
  hw_id_version_minor,
  hw_id_version_major,
  hw_id_product_major,
  hw_id_arch_patch_rev,
  hw_id_arch_minor_rev,
  hw_id_arch_major_rev,

  # struct ethosu_uapi_device_hw_cfg
  hw_cfg_macs_per_cc,
  hw_cfg_cmd_stream_version,
  hw_cfg_custom_dma,

  driver_patch_rev,
  driver_minor_rev,
  driver_major_rev ) = struct.unpack('<13I', buf)

if (all([v == 0 for v in rsp])):
  print("No response? (all zeroes)")
  print("FAILED")
  exit(2)

print("Done. Driver and Ethos-U NPU appear to be working.")
print(f">> Ethos-U driver rev. {driver_major_rev}.{driver_minor_rev}.{driver_patch_rev}")
print(f">> Product version {hw_id_product_major}.{hw_id_version_major}.{hw_id_version_minor}.{hw_id_version_status}")
print(f">> Arch rev. {hw_id_arch_major_rev}.{hw_id_arch_minor_rev}.{hw_id_arch_minor_rev}")

print("SUCCESS")
