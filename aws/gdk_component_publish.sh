#!/bin/bash
set -e
cd $1
gdk component publish 2>&1 | tee publish.log
VERSION=`grep "Created private version" publish.log | sed "s/.*version '//" | sed "s/'.*//"`
echo ${VERSION} >published_version
