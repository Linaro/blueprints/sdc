#!/bin/env python3
#
# Perform some AWS Greengrass cleanup remotely:
#
# - Delete inactive deployment revisions

import argparse
import boto3

client = boto3.client('greengrassv2')
region = client.meta.region_name
account_id = boto3.resource('iam').CurrentUser().arn.split(':')[4]


def list_device_names() -> list:
    return [device.get('coreDeviceThingName') for device in
            client.list_core_devices(maxResults=100).get('coreDevices')]


def list_device_arns() -> list:
    # ':' is not valid in a thing name, yet I managed to create one such device
    # Ignore it to avoid an error
    return [f"arn:aws:iot:{region}:{account_id}:thing/{name}" for name in
            list_device_names() if ':' not in name]


def list_deployments_for_device(arn: str) -> list:
    return [d for d in
            client.list_deployments(targetArn=arn,
                                    historyFilter='ALL',
                                    maxResults=100).get('deployments', [])]


def list_deployments() -> list:
    return [d for arn in list_device_arns() for d in
            list_deployments_for_device(arn)]


def delete_deployments(deployments: list) -> None:
    for d in deployments:
        deploymentId = d.get('deploymentId')
        print(f"  {deploymentId}", flush=True)
        client.cancel_deployment(deploymentId=deploymentId)
        client.delete_deployment(deploymentId=deploymentId)


def list_components() -> list:
    return [c for c in
            client.list_components(maxResults=100).get('components', [])]


def list_component_versions() -> list:
    return [cv for comp in list_components() for cv in
            client.list_component_versions(arn=comp.get('arn'),
                                           maxResults=100)
            .get('componentVersions')]


def delete_components(components: list) -> None:
    for c in components:
        arn = c.get('arn')
        print(f"  {arn}", flush=True)
        client.delete_component(arn=arn)


def main() -> None:
    print("Deleting all deployments")
    delete_deployments(list_deployments())
    print("Deleting all components")
    delete_components(list_component_versions())


if __name__ == '__main__':
    main()
