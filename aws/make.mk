# AWS-related target used by several other targets

aws-s3-create: out/.stamp_aws-s3-create

out/.stamp_aws-s3-create: out/.stamp_aws-create-sagemaker-role | $(builder-image)
	$(call make-in-builder,_aws-s3-create)
	$(q)touch $@

_aws-s3-create: aws/S3BucketPermissions.json
	@$(quiet) "  CREATE  s3://$(AWS_S3_BUCKET)"
	$(q)aws s3 ls s3://$(AWS_S3_BUCKET) >/dev/null 2>&1 || aws s3 mb s3://$(AWS_S3_BUCKET)
	$(q)aws s3api put-bucket-policy --bucket $(AWS_S3_BUCKET) --policy file://aws/S3BucketPermissions.json

aws-create-sagemaker-role: out/.stamp_aws-create-sagemaker-role
out/.stamp_aws-create-sagemaker-role: | $(builder-image)
	$(call make-in-builder,_aws-create-sagemaker-role)
	$(q)touch $@

_aws-create-sagemaker-role:
	@$(quiet) "  CREATE  SageMakerRole"
	$(q)aws iam get-role --role-name SageMakerRole >/dev/null 2>&1 || \
		aws iam create-role --role-name SageMakerRole \
			--assume-role-policy-document file://$(t)/aws/SageMakerRole.json
	$(q)aws iam attach-role-policy --role-name SageMakerRole \
		--policy-arn arn:aws:iam::aws:policy/AmazonSageMakerFullAccess

greengrass-put-user-policy: out/.stamp_greengrass-put-user-policy
out/.stamp_greengrass-put-user-policy: aws/SmartCameraGreengrassPolicy.json | $(builder-image)
	$(call make-in-builder,_greengrass-put-user-policy)
	$(q)touch $@

_greengrass-put-user-policy:
	@$(quiet)  "  PUT    SmartCameraGreengrassPolicy"
	$(q)aws iam put-user-policy --user-name $(AWS_USER_NAME) \
		--policy-name SmartCameraGreengrassPolicy \
		--policy-document file://aws/SmartCameraGreengrassPolicy.json

greengrass-attach-role-policy: out/.stamp_greengrass-attach-role-policy
out/.stamp_greengrass-attach-role-policy: aws/SmartCameraGreengrassComponentArtifactPolicy.json | $(builder-image)
	$(call make-in-builder,_greengrass-attach-role-policy)
	$(q)touch $@

comp-artifact-policy := arn:aws:iam::$(AWS_ACCOUNT_ID):policy/SmartCameraGreengrassComponentArtifactPolicy
_greengrass-attach-role-policy:
	@$(quiet)  "  ATTACH SmartCameraGreengrassComponentArtifactPolicy"
	$(q)aws iam get-policy \
		--policy-arn $(comp-artifact-policy) >/dev/null 2>&1 || true
	$(q)aws iam detach-role-policy \
		--policy-arn $(comp-artifact-policy) \
		--role-name SC-GreengrassV2TokenExchangeRole >/dev/null 2>&1 || true
	$(q)aws iam delete-policy --policy-arn $(comp-artifact-policy) >/dev/null 2>&1 || true
	$(q)aws iam create-policy \
		--policy-name SmartCameraGreengrassComponentArtifactPolicy \
		--policy-document file://aws/SmartCameraGreengrassComponentArtifactPolicy.json
	$(q)aws iam attach-role-policy \
		--role-name SC-GreengrassV2TokenExchangeRole \
		--policy-arn $(comp-artifact-policy)

out/greengrass-thing-name.txt: FORCE
	$(q)if [ ! "$(THING)" ]; then \
		echo 'Error: THING is not defined!'; \
		echo 'Please add "THING=<thing name>" to the make command line or to conf.mk'; \
		echo 'or to the environment.'; \
		echo 'The hex string corresponds to the MAC address of the first network adapter'; \
		echo '(such as SC-3601f76567c7). It can be found on the device'; \
		echo 'in ~root/greengrass-thing-name.txt after running "sc-demo greengrass-install"'; \
		false; \
	fi
	@$(quiet) "  CHECK  $@"
	$(q)echo "$(THING)" >$@.tmp
	$(q)$(call mv-if-changed,$@.tmp,$@)

sgg := out/staging/greengrassv2

aws/GreengrassDeployment.json: aws/GreengrassDeployment.json.in out/greengrass-thing-name.txt \
				$(sgg)/SmartCameraApplication/published_version \
				$(sgg)/SmartCameraCapture/published_version \
				$(sgg)/SmartCameraCloudAgent/published_version \
				$(sgg)/SmartCameraCloudWatch/published_version \
				$(sgg)/SmartCameraInference/published_version \
				$(sgg)/SmartCameraTest/published_version
	@$(quiet) "  GEN    $@"
	$(q)export APPLICATION_VERSION=`cat $(sgg)/SmartCameraApplication/published_version`; \
		export CAPTURE_VERSION=`cat $(sgg)/SmartCameraCapture/published_version`; \
		export CLOUDAGENT_VERSION=`cat $(sgg)/SmartCameraCloudAgent/published_version`; \
		export CLOUDWATCH_VERSION=`cat $(sgg)/SmartCameraCloudWatch/published_version`; \
		export INFERENCE_VERSION=`cat $(sgg)/SmartCameraInference/published_version`; \
		export TEST_VERSION=`cat $(sgg)/SmartCameraTest/published_version`; \
		$(call process-in-file,aws/GreengrassDeployment.json.in,>$@)

greengrass-deployment: aws/GreengrassDeployment.json
	$(call make-in-builder,_greengrass-deployment)

_greengrass-deployment:
	@$(quiet) "  AWSCLI create-deployment"
	$(q)aws greengrassv2 create-deployment --cli-input-json file://aws/GreengrassDeployment.json

greengrass-cleanup:
	$(call run-builder,aws/cleanup.py)
