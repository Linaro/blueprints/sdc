import tensorflow as tf

from helper.losses_utils.iou_losses import compute_CIoU_loss

WEIGHT_DECAY = 0.  # 5e-4
LEAKY_ALPHA = 0.1


def myConv2D(*args, **kwargs):
    darknet_conv_kwargs = {"kernel_regularizer": tf.keras.regularizers.l2(WEIGHT_DECAY),
                           "kernel_initializer": tf.keras.initializers.RandomNormal(mean=0.0, stddev=0.01),
                           "padding": "valid" if kwargs.get(
                               "strides") == (2, 2) else "same"}
    darknet_conv_kwargs.update(kwargs)

    return tf.keras.layers.Conv2D(*args, **darknet_conv_kwargs)


def myConv2D_BN_Leaky(*args, **kwargs):
    without_bias_kwargs = {"use_bias": False}
    without_bias_kwargs.update(kwargs)

    def wrapper(x):
        x = myConv2D(*args, **without_bias_kwargs)(x)
        x = tf.keras.layers.BatchNormalization()(x)
        x = tf.keras.layers.LeakyReLU(alpha=LEAKY_ALPHA)(x)
        return x

    return wrapper


def myBlock(filters, niter):
    def wrapper(x):
        x = tf.keras.layers.ZeroPadding2D(((1, 0), (1, 0)))(x)  # top left half-padding
        x = myConv2D_BN_Leaky(filters, (3, 3), strides=(2, 2))(x)
        for _ in range(niter):
            y = myConv2D_BN_Leaky(filters // 2, (1, 1))(x)
            y = myConv2D_BN_Leaky(filters, (3, 3))(y)
            x = tf.keras.layers.Add()([x, y])
        return x

    return wrapper


def model(iou_threshold, score_threshold, max_outputs, num_classes, strides, mask, anchors,
                input_size=None,
                name=None):
    if input_size is None:
        x = inputs = tf.keras.Input([None, None, 3])
    else:
        x = inputs = tf.keras.Input([input_size, input_size, 3])

    #x = PreprocessInput()(x)

    x = myConv2D_BN_Leaky(32, 3)(x)  # 0
    x = myBlock(64, 1)(x)  # 1-4
    x = myBlock(128, 2)(x)  # 5-11
    x = x_36 = myBlock(256, 8)(x)  # 12-36
    x = x_61 = myBlock(512, 8)(x)  # 37-61
    x = myBlock(1024, 4)(x)  # 62-74

    x = myConv2D_BN_Leaky(512, 1)(x)  # 75
    x = myConv2D_BN_Leaky(1024, 3)(x)  # 76
    x = myConv2D_BN_Leaky(512, 1)(x)  # 77
    x = myConv2D_BN_Leaky(1024, 3)(x)  # 78

    x = x_79 = myConv2D_BN_Leaky(512, 1)(x)  # 79
    x = myConv2D_BN_Leaky(1024, 3)(x)  # 80
    output_0 = myConv2D(len(mask[0]) * (num_classes + 5), 1)(x)

    x = myConv2D_BN_Leaky(256, 1)(x_79)
    x = tf.keras.layers.UpSampling2D(2)(x)
    x = tf.keras.layers.Concatenate()([x, x_61])

    x = myConv2D_BN_Leaky(256, 1)(x)
    x = myConv2D_BN_Leaky(512, 3)(x)
    x = myConv2D_BN_Leaky(256, 1)(x)
    x = myConv2D_BN_Leaky(512, 3)(x)
    x = x_91 = myConv2D_BN_Leaky(256, 1)(x)

    x = myConv2D_BN_Leaky(512, 3)(x)
    output_1 = myConv2D(len(mask[1]) * (num_classes + 5), 1)(x)

    x = myConv2D_BN_Leaky(128, 1)(x_91)
    x = tf.keras.layers.UpSampling2D(2)(x)
    x = tf.keras.layers.Concatenate()([x, x_36])

    x = myConv2D_BN_Leaky(128, 1)(x)
    x = myConv2D_BN_Leaky(256, 3)(x)
    x = myConv2D_BN_Leaky(128, 1)(x)
    x = myConv2D_BN_Leaky(256, 3)(x)
    x = myConv2D_BN_Leaky(128, 1)(x)
    x = myConv2D_BN_Leaky(256, 3)(x)

    output_2 = myConv2D(len(mask[2]) * (num_classes + 5), 1)(x)
    model = tf.keras.Model(inputs, (output_0, output_1, output_2), name=name)


    return model


class PreprocessInput(tf.keras.layers.Layer):

    def __init__(self, **kwargs):
        super(PreprocessInput, self).__init__(**kwargs)

    def build(self, input_shape):
        super(PreprocessInput, self).build(input_shape)  # Be sure to call this at the end

    def call(self, inputs, **kwargs):
        x = tf.divide(inputs, 255.)
        return x

    def compute_output_shape(self, input_shape):
        return input_shape



def _broadcast_iou(box_1, box_2):
    # box_1: (..., (x1, y1, x2, y2))
    # box_2: (N, (x1, y1, x2, y2))

    # broadcast boxes
    box_1 = tf.expand_dims(box_1, -2)
    box_2 = tf.expand_dims(box_2, 0)
    # new_shape: (..., N, (x1, y1, x2, y2))
    new_shape = tf.broadcast_dynamic_shape(tf.shape(box_1), tf.shape(box_2))
    box_1 = tf.broadcast_to(box_1, new_shape)
    box_2 = tf.broadcast_to(box_2, new_shape)

    int_w = tf.maximum(tf.minimum(box_1[..., 2], box_2[..., 2]) - tf.maximum(box_1[..., 0], box_2[..., 0]), 0)
    int_h = tf.maximum(tf.minimum(box_1[..., 3], box_2[..., 3]) - tf.maximum(box_1[..., 1], box_2[..., 1]), 0)
    int_area = int_w * int_h
    box_1_area = (box_1[..., 2] - box_1[..., 0]) * (box_1[..., 3] - box_1[..., 1])
    box_2_area = (box_2[..., 2] - box_2[..., 0]) * (box_2[..., 3] - box_2[..., 1])
    return int_area / tf.maximum(box_1_area + box_2_area - int_area, 1e-8)


def modelLoss(anchors, stride, num_classes, ignore_thresh,type):
    def wrapper(y_true, y_pred):
        # 0. default
        dtype = y_pred.dtype
        y_shape = tf.shape(y_pred)
        grid_w, grid_h = y_shape[2], y_shape[1]
        anchors_tensor = tf.cast(anchors, dtype)
        y_true = tf.reshape(y_true, (y_shape[0], y_shape[1], y_shape[2], anchors_tensor.shape[0], num_classes + 5))
        y_pred = tf.reshape(y_pred, (y_shape[0], y_shape[1], y_shape[2], anchors_tensor.shape[0], num_classes + 5))

        # 1. transform all pred outputs
        # y_pred: (batch_size, grid, grid, anchors, (x, y, w, h, obj, ...cls))
        pred_xy, pred_wh, pred_obj, pred_cls = tf.split(y_pred, (2, 2, 1, num_classes), axis=-1)

        # !!! grid[x][y] == (y, x)
        grid = tf.meshgrid(tf.range(grid_w), tf.range(grid_h))
        grid = tf.expand_dims(tf.stack(grid, axis=-1), axis=2)  # [gx, gy, 1, 2]

        pred_xy = (tf.sigmoid(pred_xy) + tf.cast(grid, dtype)) * stride
        pred_wh = tf.exp(pred_wh) * anchors_tensor
        pred_obj = tf.sigmoid(pred_obj)
        pred_cls = tf.sigmoid(pred_cls)

        pred_wh_half = pred_wh / 2.
        pred_x1y1 = pred_xy - pred_wh_half
        pred_x2y2 = pred_xy + pred_wh_half
        pred_box = tf.concat([pred_x1y1, pred_x2y2], axis=-1)

        # 2. transform all true outputs
        # y_true: (batch_size, grid, grid, anchors, (x1, y1, x2, y2, obj, cls))
        true_box, true_obj, true_cls = tf.split(y_true, (4, 1, num_classes), axis=-1)
        true_xy = (true_box[..., 0:2] + true_box[..., 2:4]) / 2.
        true_wh = true_box[..., 2:4] - true_box[..., 0:2]

        # give higher weights to small boxes
        box_loss_scale = 2 - true_wh[..., 0] * true_wh[..., 1] / (tf.cast(tf.reduce_prod([grid_w, grid_h, stride, stride]), dtype))

        # 4. calculate all masks
        obj_mask = tf.squeeze(true_obj, -1)
        # ignore false positive when iou is over threshold
        best_iou = tf.map_fn(
            lambda x: tf.reduce_max(_broadcast_iou(x[0], tf.boolean_mask(x[1], tf.cast(x[2], tf.bool))), axis=-1),
            (pred_box, true_box, obj_mask),
            dtype)
        ignore_mask = tf.cast(best_iou < ignore_thresh, dtype)

        # 5. calculate all losses
        
        ciou = compute_CIoU_loss(pred_box, true_box)
        box_loss = 1. - ciou
        

        box_loss = obj_mask * box_loss_scale * box_loss
        obj_loss = tf.keras.losses.binary_crossentropy(true_obj, pred_obj)
        obj_loss = obj_mask * obj_loss + (1 - obj_mask) * ignore_mask * obj_loss
        cls_loss = obj_mask * tf.keras.losses.binary_crossentropy(true_cls, pred_cls)

        def _focal_loss(y_true, y_pred, alpha=1, gamma=2):
            focal_loss = tf.squeeze(alpha * tf.pow(tf.abs(y_true - y_pred), gamma), axis=-1)
            return focal_loss

        
        focal_loss = _focal_loss(true_obj, pred_obj)
        obj_loss = focal_loss * obj_loss

        # 6. sum over (batch, gridx, gridy, anchors) => (batch, 1)
        box_loss = tf.reduce_mean(tf.reduce_sum(box_loss, axis=(1, 2, 3)))
        obj_loss = tf.reduce_mean(tf.reduce_sum(obj_loss, axis=(1, 2, 3)))
        cls_loss = tf.reduce_mean(tf.reduce_sum(cls_loss, axis=(1, 2, 3)))

        return box_loss + obj_loss + cls_loss

    return wrapper


