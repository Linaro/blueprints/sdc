from pycocotools.coco import COCO
import requests
import os
import sys
import threading
import cv2


def makeDirectory(dirName):
    try:
        os.makedirs(dirName)
        print(f"Created {dirName}")
    except FileExistsError:
        pass


def getImagesFromClassName(className):
    global coco
    makeDirectory(f'out/downloaded_images/{className}')
    catIds = coco.getCatIds(catNms=[className])
    imgIds = coco.getImgIds(catIds=catIds )
    images = coco.loadImgs(imgIds)

    symlinks_created = 0
    label_files_created = 0
    print(f"[Thread start] Checking/creating symlinks/label files for images in class '{className}' ({len(images)} images)...")
    for im in images:
        image_file_name = im['file_name']
        label_file_name = im['file_name'].split('.')[0] + '.txt'

        fileExists = os.path.exists(f'out/downloaded_images/{className}/{image_file_name}')
        labelFileExists = os.path.exists(f'out/downloaded_images/{className}/{label_file_name}')
        if (not fileExists or not labelFileExists):
            
            #try:
            #    img_data = requests.get(im['coco_url']).content
            #except Exception as e:
            #    try:
            #        print("Retrying to download image ", image_file_name)
            #        img_data = requests.get(im['coco_url']).content
            #    except Exception as e:
            #        print(className, ": Exception occurred while downloading ", image_file_name, "   Skipping this image....")
            #        continue

            annIds = coco.getAnnIds(imgIds=im['id'], catIds=catIds, iscrowd=None)
            anns = coco.loadAnns(annIds)    
            s = ""            
            for i in range(len(anns)):
                xmin = int(anns[i]['bbox'][0])
                ymin = int(anns[i]['bbox'][1])
                xmax = int(xmin + anns[i]['bbox'][2])
                ymax = int(ymin + anns[i]['bbox'][3])            
                s += "0 " + str(xmin) + " " + \
                            str(ymin) + " " + \
                            str(xmax) + " " + \
                            str(ymax) + " "
                
                if(i < len(anns) - 1):
                    s += '\n'
           
            if (not fileExists):
                os.symlink(f'../../train2017/{image_file_name}', f'out/downloaded_images/{className}/{image_file_name}');
                symlinks_created = symlinks_created + 1
            #with open(f'downloaded_images/{className}/{image_file_name}', 'wb') as image_handler:
            #    image_handler.write(img_data)
            if (not labelFileExists):
                with open(f'out/downloaded_images/{className}/{label_file_name}', 'w') as label_handler:
                    label_handler.write(s)
                label_files_created = label_files_created + 1

    print(f"[Thread end] Class '{className}': {symlinks_created}/{label_files_created} symlinks/label files created")

coco = COCO('out/instances_train2017.json')

def download_images_from_coco(classes):
    global coco
    classes = [class_name.lower() for class_name in classes] # Converting to lower case

    if(classes[0] == "--help"):
        with open('classes.txt', 'r') as fp:
            lines = fp.readlines()
        print("**** Classes ****\n")
        [print(x.split('\n')[0]) for x in lines]
        exit(0)

    print("\nClasses to download: ", classes, end = "\n\n")

    makeDirectory('out/downloaded_images')

    cats = coco.loadCats(coco.getCatIds())
    nms=[cat['name'] for cat in cats]

    for name in classes:
        if(name not in nms):
            print(f"{name} is not a valid class, Skipping.")
            classes.remove(name)

    threads = []

    for i in range(len(classes)):
        t = threading.Thread(target=getImagesFromClassName, args=(classes[i],)) 
        threads.append(t)
        
    for t in threads:
        t.start()

    for t in threads:
        t.join()

    print("Done.")
