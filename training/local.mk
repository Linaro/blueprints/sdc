t := training

training-dataset-build: out/.stamp_training-dataset-build

training-annotations-dl: out/instances_train2017.json

training-images-dl: out/.stamp_training-images-dl

training-downloader-image: out/.stamp_training-downloader-image

training-local-image: out/.stamp_training-local-image

training-local: out/$(TRAINING_MODEL)/fused_model.onnx

training: training-local

out/annotations_trainval2017.zip: | $(builder-image)
	@$(quiet) "  DL      $@"
	$(call run-builder,curl -o $@ http://images.cocodataset.org/annotations/annotations_trainval2017.zip)
	$(q)touch $@

out/instances_train2017.json: out/annotations_trainval2017.zip | $(builder-image)
	@$(quiet) "  UNZIP   $@"
	$(call run-builder,unzip -u -q -j $< annotations/instances_train2017.json -d out)
	$(q)touch $@

out/train2017.zip: | $(builder-image)
	@$(quiet) "  DL      $@"
	$(call run-builder,lftp -c 'lcd out; pget -n 20 -c http://images.cocodataset.org/zips/train2017.zip')
	$(q)touch $@

out/.stamp_training-images-dl: out/train2017.zip | $(builder-image)
	@$(quiet) "  UNZIP   out/train2017"
	$(call run-builder,unzip -u -q $< -d out)
	$(q)touch $@

out/.stamp_training-dataset-builder-image: $(t)/dataset_downloader/Dockerfile
	@$(quiet) "  DOCKBLD sc-downloader"
	$(q)docker build $(DOCKER_BUILD_OPTS) \
		--build-arg HOST_DOCKER_GID=$$(stat -c %g /var/run/docker.sock 2>/dev/null || echo 0) \
		--build-arg USER_ID=$$(id -u) \
		--build-arg USER_GID=$$(id -g) \
		-t sc-dataset-builder $(t)/dataset_downloader
	$(q)touch $@

out/.stamp_training-dataset-build: out/.stamp_training-dataset-builder-image \
					out/instances_train2017.json \
					out/.stamp_training-images-dl \
					| $(builder-image)
	$(call mkdir,out/dataset)
	@$(quiet) "  GEN     out/dataset"
	$(call run-builder,python3 training/dataset_downloader.py,sc-dataset-builder)
	$(q)touch $@

out/.stamp_training-local-image: $(t)/local/Dockerfile
	$(q)if [ $$(uname -m) != x86_64 ]; then echo "x86_64 host required"; false; fi
	@$(quiet) "  DOCKBLD sc-training-local"
	$(q)docker build $(DOCKER_BUILD_OPTS) \
		--build-arg HOST_DOCKER_GID=$$(stat -c %g /var/run/docker.sock 2>/dev/null || echo 0) \
		--build-arg USER_ID=$$(id -u) \
		--build-arg USER_GID=$$(id -g) \
		-t sc-training-local $(t)/local
	$(q)touch $@

out/$(TRAINING_MODEL)/fused_model.onnx: out/.stamp_training-local-image \
					out/.stamp_training-dataset-build \
					 | $(builder-image)
	$(call mkdir,out/$(TRAINING_MODEL))
	$(call run-builder,-m $(TRAINING_MODEL) -e $(TRAINING_NUMBER_OF_EPOCHS) --local,sc-training-local)
