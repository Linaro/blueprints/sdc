import numpy as np
import json
import os
from tensorflow.keras import optimizers
from tensorflow.keras.callbacks import ModelCheckpoint, ReduceLROnPlateau, EarlyStopping
from helper.dataset_utils.dataset_helpers import Create_Dataset, decode_class_names
import argparse
import tensorflow
import tflite
import onnx
import tensorflow as tf
from tensorflow.python.framework import convert_to_constants
import tf2onnx
import onnx
import onnxruntime
import onnxoptimizer
from tensorflow.python.tools import optimize_for_inference_lib
from tensorflow.keras.layers import Layer, InputSpec
from tensorflow.compat.v1.keras import backend as K
tf.keras.backend.set_learning_phase(0)
from tensorflow.python.platform import gfile
from tensorflow.core.protobuf import saved_model_pb2
from tensorflow.python.util import compat
from tensorflow.keras.models import load_model

ap = argparse.ArgumentParser()
ap.add_argument("-e", "--epochs", required=True,help="Number of epochs")
ap.add_argument("-m", "--model_type", required=True,help="Model_type supported models yolov3/tiny_yolov3")
ap.add_argument("-c", "--cloud", default="" ,help="Supported clouds are AWS/Alibaba")
ap.add_argument("-l", "--local",  action="store_true",help="Save the model locally")
ap.add_argument("-t","--export_as",default=".pb",required=True,help="Export the trained model as .pb/ .onnx")
args = vars(ap.parse_args())

class Train_model:
    def __init__(self, model_type, epochs):
        self.model_type = model_type
        self.epochs = epochs
        self.image_size = "416"
        self.class_name_path ="training/model_data/classes.txt"
        self.anno_path = "out/dataset/train.txt"
        self.loss_type = "CIoU+FL"
        self.iou_threshold = 0.45
        self.ignore_threshold = 0.7
        self.score_threshold = 0.6
        self.max_outputs = 100
        self.batch_size = 4


    def load_model_config(self):
        if self.model_type == "yolov3":
            print("Loading YOLOV3 configuration")
            from helper.model.yolov3 import model as Model
            from helper.model.yolov3 import modelLoss as Loss
            self.Model = Model
            self.Loss = Loss
            self.strides =  "32,16,8"
            self.anchors =  "10,13 16,30 33,23 30,61 62,45 59,119 116,90 156,198 373,326"
            self.mask= "6,7,8 3,4,5 0,1,2"
            self.init_weight = "out/yolov3/checkpoint.h5"

        elif self.model_type == "tiny_yolov3":
            print("Loading Tiny YOLOV3 configuration")
            from helper.model.tiny_model import my_model as Model
            from helper.model.tiny_model import modelLoss as Loss
            self.Model = Model
            self.Loss = Loss
            self.strides = "32,16"
            self.anchors = "23,27 37,58 81,82 81,82 135,169 344,319"
            self.mask = "3,4,5 0,1,2"
            self.init_weight = "out/tiny_yolov3/checkpoint.h5"
        else:
            print("Given model not supported\nSupported models are yolov3/tiny_yolov3\nexiting.....")
            exit(0)

    def load_train_configuration(self):
        num_classes = len(decode_class_names(self.class_name_path))
        anchors = np.array(list(map(lambda x: list(map(int, str.split(x, ','))), self.anchors.split())))
        mask = np.array(list(map(lambda x: list(map(int, str.split(x, ','))), self.mask.split())))
        strides = list(map(int,self. strides.split(',')))
        image_size = list(map(int, self.image_size.split(',')))
        model = self.Model(self.iou_threshold, self.score_threshold, self.max_outputs, num_classes, strides, mask, anchors)
        train_dataset = Create_Dataset(mask, anchors, self.max_outputs, strides, self.class_name_path, self.anno_path, image_size, self.batch_size)

        try:
            model.load_weights(self.init_weight, by_name=True, skip_mismatch=True)
            print("################ weights loaded: ",  self.init_weight,"#######################")
        except:
            print("################ No weight file could be loaded")

        loss = [self.Loss(anchors[mask[i]],
                    strides[i],
                    train_dataset.num_classes,
                    self.ignore_threshold,
                    self.loss_type) for i in range(len(mask))]

        model_type = args["model_type"]
        checkpoint = ModelCheckpoint(f'out/{model_type}/checkpoint.h5',
            monitor='loss', save_weights_only=True, save_best_only=True, period=1)
        reduce_lr = ReduceLROnPlateau(monitor='loss', factor=0.1, patience=4, verbose=1)
        early_stopping = EarlyStopping(monitor='loss', min_delta=0, patience=20, verbose=1)

        with open('out/dataset/train.txt') as f:
            lines_train = f.readlines()
        np.random.seed(10101)
        np.random.shuffle(lines_train)
        np.random.seed(None)
        num_train = len(lines_train)
        self.train_config = [num_train,loss,train_dataset,checkpoint,reduce_lr,early_stopping,model]

    def train_model(self):
        num_train,loss,train_dataset,checkpoint,reduce_lr,early_stopping,model = self.train_config
        for i in range(len(model.layers)):
            model.layers[i].trainable = True
        model.compile(loss=loss, optimizer=optimizers.Adam(lr=1e-4), run_eagerly=False) # recompile to apply the change
        print('Unfreeze all of the layers.')
        model.fit(train_dataset,
            #steps_per_epoch=1,
            epochs=int(self.epochs),
            initial_epoch=0,
            callbacks=[checkpoint, reduce_lr, early_stopping])

        return model

def convert_the_model(model,model_name):
     var = "trained_model.h5"
     model.save(var)
     output_nodes = ["model/" + node.name.split(":")[0] for node in model.outputs]
     print("output_nodes:",output_nodes)
     #Convert the model from Keras h5 format to Tensorflow frozen graph(pb) format
     inference_func = tensorflow.function(lambda input_1: model(input_1))
     concrete_func = inference_func.get_concrete_function(tensorflow.TensorSpec([1, 416, 416, 3], tf.float32))
     output_func = convert_to_constants.convert_variables_to_constants_v2(concrete_func)
     graph_def = output_func.graph.as_graph_def()
     if args["export_as"] == ".pb":
         model_to_export = f"{model_name}.pb"
         tensorflow.io.write_graph(graph_or_graph_def=output_func.graph, logdir="./frozen_models", name= model_to_export, as_text=False)
     elif args["export_as"] == ".onnx":
         inference_func = tensorflow.function(lambda input_1: model(input_1))
         concrete_func = inference_func.get_concrete_function(tensorflow.TensorSpec(model.inputs[0].shape, model.inputs[0].dtype))
         output_func = convert_to_constants.convert_variables_to_constants_v2(concrete_func)
         graph_def = output_func.graph.as_graph_def()
         tensorflow.io.write_graph(graph_or_graph_def=output_func.graph, logdir="./frozen_models", name="frozen_graph.pb", as_text=False)
         converter = tensorflow.compat.v1.lite.TFLiteConverter.from_frozen_graph("./frozen_models/frozen_graph.pb",
                                                                         input_arrays=["input_1"],
                                                                         output_arrays=output_nodes)

         tfmodel = converter.convert()
         open("saved_model.tflite" , "wb").write(tfmodel)
         print("Converted to tflite_format")

         #Convert the model from tflite to onnx format
         tf2onnx.convert.from_tflite("saved_model.tflite", output_path="saved_model.onnx")

         #Convert the model from onnx to optimized onnx format
         onnx_model = onnx.load("saved_model.onnx")
         model_fuse_bn = onnxoptimizer.optimize(onnx_model, ['fuse_bn_into_conv',
                                 'eliminate_unused_initializer', 'fuse_add_bias_into_conv',
                                 'fuse_consecutive_concats', 'fuse_consecutive_reduce_unsqueeze',
                                 'fuse_consecutive_squeezes', 'fuse_consecutive_transposes',
                                 'fuse_matmul_add_bias_into_gemm', 'fuse_pad_into_conv', 'fuse_transpose_into_gemm'] )
         onnx.save_model(model_fuse_bn, "trained_model.onnx")

def run():
    trainer = Train_model(args["model_type"], args["epochs"])
    trainer.load_model_config()
    trainer.load_train_configuration()
    trained_model = trainer.train_model()
    convert_the_model(trained_model,args["model_type"])

    if args["cloud"] == "AWS":
        print("Training done. TBD: copy files to /opt/ml/model/output/");
    elif args["cloud"] == "" and args["local"]:
        save_model_locally(trained_model)
    else:
        print("Cloud platform not supported...")

if __name__ == "__main__":
    run()
