staging-gg-cloudwatch := out/staging/greengrassv2/SmartCameraCloudWatch

greengrass-cloudwatch-component: $(staging-gg-cloudwatch)/zip-build/SmartCameraCloudWatch.zip

$(staging-gg-cloudwatch)/zip-build/SmartCameraCloudWatch.zip: $(staging-gg-cloudwatch)/sc-cloudwatch.tar.gz \
				$(staging-gg-cloudwatch)/gdk-config.json \
				$(staging-gg-cloudwatch)/recipe.yaml | $(builder-image)
	@$(quiet) "  BUILD   $@"
	$(q)$(call run-builder,bash -c 'cd $(staging-gg-cloudwatch) && gdk component build')

out/.stamp_cloudwatch: cloudwatch/Dockerfile cloudwatch/lambda.py | $(builder-image)
	$(call run-builder,docker buildx build $(DOCKER_BUILD_OPTS) --platform aarch64 -t sc-cloudwatch cloudwatch)
	$(q)touch $@

out/sc-cloudwatch.tar.gz: out/.stamp_cloudwatch | $(builder-image)
	 @$(quiet) "  DOCKSAV $@"
	 $(q)$(call run-builder,bash -c 'docker save sc-cloudwatch | pigz >$@')

$(staging-gg-cloudwatch)/sc-cloudwatch.tar.gz: out/sc-cloudwatch.tar.gz
	$(call cp-file)

gg-cw := cloudwatch/greengrassv2/SmartCameraCloudWatch

$(staging-gg-cloudwatch)/gdk-config.json: $(gg-cw)/gdk-config.json
	$(call cp-file)

$(staging-gg-cloudwatch)/recipe.yaml: $(gg-cw)/recipe.yaml
	$(call cp-file)

# TBD: check/update policies
greengrass-cloudwatch-component-publish: $(staging-gg-cloudwatch)/zip-build/SmartCameraCloudWatch.zip \
				out/.stamp_greengrass-attach-role-policy | $(builder-image)
	@$(quiet) "  PUBLISH $(notdir $<)"
	$(q)$(call run-builder,./aws/gdk_component_publish.sh $(staging-gg-cloudwatch))

greengrass-components: greengrass-cloudwatch-component

greengrass-components-publish: greengrass-cloudwatch-component-publish
