# Linaro Software Defined Camera (Smart Camera v2)

## Supported boards

- Rockpi4B+
- AMD/Xilinx Kria KV260

## How to clone the project

```
$ sudo apt install docker.io git make
$ sudo usermod -aG docker $(whoami) # then logout and login again
$ git clone --recurse-submodules https://gitlab.com/Linaro/blueprints/sdc
```

## Pre-built firmware and root FS images

### RockPi4 pre-built images

Run `make rockpi4-trs-images-download` to retrieve the firmware and OS images
from the latest Linaro TRS build, then:
- `make rockpi4-trs-firmware-flash DEV=/dev/mmcblkX` to flash the firmware to
a microSD card
- `make rockpi4-trs-rootfs-flash DEV=/dev/sdX` to flash the OS to a USB stick.

If the USB storage device is not detected (either by U-Boot or by the kernel)
try a different port (do try all 4 ports, the two black ones and the two blue
ones, the blue are preferred because they are faster being USB-3).

### AMD/Xilinx Kria KV260 pre-built images

The latest Linaro firmware and OS images can be downloaded via
`make kv260-trs-images-download`. The firware image is saved as
`out/kv260/trs/ImageA.bin` while the root FS image is
`out/trs-image-trs-qemuarm64.rootfs.wic.gz` (please ignore the `qemu64` part,
it is a common image for all arm64 devices).

- The firmware (`ImageA.bin`) needs to be installed manually thanks to the
board's built-in HTTP server. Power up the board while keeping the FUWEN button
depressed, then connect to http://192.168.0.111/ and upload the firmware to one
of the two boot slots (A or B).
- The root FS should be written to a micro SD card with
`make rockpi4-trs-rootfs-flash DEV=/dev/mmcblkX` (or whatever device your SD
card shows up as).

The KV260 serial console is on the second USB device that gets enumerated when
the board is connected, that is: `/dev/ttyUSB1` assuming no other TTY USB
devices are present. The speed is 115200 bauds. I use `pyserial-miniterm` from
the Ubuntu package `python3-serial`:

```
pyserial-miniterm --raw --eol CR /dev/ttyUSB1 115200
```

## How to build and run the Software Defined Camera stack (without AWS Greengrass)

This section assumes the target device runs a SystemReady-compliant firmware and
Linux image. On some Linux distributions you may need to do a few things. First,
run `passwd -d root` to delete the root password. Then add:
```
PermitRootLogin yes
PermitEmptyPasswords yes
```
...to `/etc/ssh/sshd_config`. Then run `systemctl restart ssh`. These steps allow
the `deploy` command to work without any authentication for convenience.

On an `x86_64` or `aarch64` Linux machine (Debian/Ubuntu):

```
$ cd smart-camera-v2
$ make
$ make deploy TO=<device-ip>
```
On the device:
```
# ./sc-demo install
# ./sc-demo test
[...]
>> Leaving test_detection() (PASSED)
[...]
# ./sc-demo start
[...]
Demo will be accessible at: http://192.168.0.12:5000/
```
Instead of `make deploy` and `./sc-demo install`, which depend on images
archives transfert via `scp`, the containers may be pushed to the Docker Hub
by `make images-push` (check that you have proper Docker Hub credentials in
`~/.docker` and that `DOCKER_NAMESPACE` is set to your Docker Hub account name
in `conf.mk`).

Then the containers may be deployed to the device like so:
```
$ make sc-demo
$ scp sc-demo root@<device-ip>:
```
...then run `./sc-demo install-from-dockerhub` on the device, which will pull
the containers.

## How to build and run the Software Defined Camera stack (with AWS Greengrass)

On an `x86_64` or `aarch64` Linux machine (Debian/Ubuntu):

```
$ cd smart-camera-v2
$ make sc-demo
$ scp sc-demo root@<device-ip>:
```
On the device:
```
# chmod +x sc-demo
# ./sc-demo install-greengrass
# cat greengrass-thing-name.txt
SC-3601f76567c7
```
Take note of the "Greengrass thing name", it needs to be supplied in the
next step. Back onto the build computer:
```
$ make greengrass-components-publish
$ make greengrass-deployment THING=SC-3601f76567c7
```
At this point, the software should begin to be downloaded automatically onto
the device. You can check the progress of the installation on the AWS IoT page.
This operation typically takes 5-10 minutes.
When the deployment is complete, you can run the solution by logging into the
device and running `sc-demo test` and/or `sc-demo start`.
```
# ./sc-demo test
[...]
>> Entering test_detection()
[...]
>> Leaving test_detection() (PASSED)
[...]
>> Entering test_cloudwatch()
[...]
>> Leaving test_cloudwatch() (PASSED)
# ./sc-demo start
[...]
Demo will be accessible at: http://192.168.0.12:5000/
```

## More information

The Software Defined Camera project provides reference implementation to
demonstrate a typical object recognition stack, based on a microservice
architecture and suitable for cloud deployment. The main steps are:

1. Train an AI model from the Microsoft COCO (Common Objects in Context)
   dataset
2. Compile the trained model for an Aarch64 Linux target
3. Build Docker images for the microservice containers: video capture,
   AI inference, main application, cloud agent, and (for AWS) cloudwatch
   interface and test containers.
4. Download prebuilt firmware and OS images for the target device (RockPi4
   or Xilinx Kria KV260).
   SDC runs preferably on top of the Linaro Trusted Reference Stack software,
   but other Linux distributions on top of a SystemReady hardware and firmware
   should also work.
5. Deploy the containers onto the target device and run the demo.

Steps 1 and 2 can be performed either locally or in the cloud using Amazon Web
Services (SageMaker). It is also possible to download a prebuilt model to speed
things up. The software deployment can be done manually (via `scp` with the
help of the `deploy` target), or using AWS Greengrass.

### AI model training

When `TRAINING_MODE=local`, `make training` downloads about 20 GB of data from
https://cocodataset.org. The training phase can use a GPU if available (tested
on an `x86_64` Amazon EC2 instance with NVidia T4).

When `TRAINING_MODE=aws`, `make training` invokes AWS SageMaker services.

### AI model compilation

`make compilation` converts the trained model into a compiled model executable
on the Aarch64 target device. When `COMPILATION_MODE=local`, the build happens
locally. When `COMPILATION_MODE=aws`, AWS SageMaker is used instead.

When `COMPILATION_MODE=prebuilt`, a prebuilt model is downloaded. This
effectively allows skipping both the model training and compilation steps.

### Building the inference container image

`make inference` produces an `aarch64` Docker image that contains the compiled
model. The command can be run equally on an `x86_64` or `aarch64` machine, but
`x86_64` is much slower to build due to emulation (the `docker buildx` command
uses QEMU).

`make inference-image-save` saves the image as `out/sc-inference.tar.gz`.

### Building the video capture container image

Use `make capture` and `make capture-image-save` to build the video capture
container and save it to disk as `out/sc-capture.tar.gz`.

## Installing SDC

The container images may be copied to the target device using `scp` or any
other mean. The Makefile provides a convenient `deploy` target for that:

```
$ make deploy TO=<board-ip>
```
Then, log into the board as root and run `sc-demo install` to load the
containers into Docker.
```
# ./sc-demo install
```

## Running SDC

The `sc-demo` script can be used to start and stop the demo. It also contains
a test mode (`sc-demo test`) which performs an end-to-end sanity check. It
starts the containers and uses a small video file as a source. Then it connects
to the video stream over HTTP to make sure some image is present. Finally it
queries the total count of objects that have been detected, that is over HTTP
as well. A zero status is returned if if one or more objects were detected.
Non-zero is returned otherwise.

## Amazon Web Services setup

Some actions in this project may require an AWS account, depending on how things
are configured in `conf.mk`. This sections will guide you through the account
setup process.

### Pre-requisites for registration

This process can be skipped if you already have an AWS account that fulfills
the following requirements:

1. Ability to create and manage IAM users & groups
2. Ability to create and manage IAM roles
3. Ability to grant access for AWS services to IAM users/roles

If you don't already have an AWS account, you will need to sign up for one.
Note that at the time of writing this document, the
[AWS Free Tier](https://aws.amazon.com/free/) is sufficient to run the demo.

You will need:

1. An e-mail address to register
2. A valid credit card that can be charged and verified during registration
3. A contact phone (mobile number)

To register go to https://aws.amazon.com/.

### Create IAM User

IAM (Identity and Access Management) allows to create a profile for use with
services on AWS. For the Smart Camera use case, a user and one or several roles
are needed. Only the user must be created manually; the makefiles will take
care of any subsequent step by using the user's credentials.

To create an IAM user:

1. Open the AWS dashboard
2. Search for and select `IAM - Manage access to AWS resources`
3. On the left-hand side pane select `Access management >> Users`
4. Click on the "Add users" button on the right hand side of the page
5. Pick a name for the IAM user, such as `SmartCamera` and click
   `Next`.
8. In `Set permissions`: select `Attach policies directly` then
   `Next` and finally `Create user`.
9. Select the newly created user, in the `Security credentials` tab
   click `Create access key`. Select `Command Line Interface (CLI)`.
   Tick `I understand the above recommendations [...]` and click
   `Next`.
10. Click `Create access key`
11. Take note of the following:
       - Access key ID. That's your `AWS_ACCESS_KEY_ID` in `aws.mk`.
       - Secret access key (hidden, click `Show` to reveal). This one
         is `AWS_SECRET_ACCESS_KEY` in `aws.mk`.
12. Click `Done`.
13. Back on the user summary page, take note of the numerical part of the
    user's ARN. That number is called `AWS_ACCOUNT_ID` in `aws.mk`.
14. Select the `Permissions` tab and
    click `Add permissions`/`Add inline policy`. Select the `JSON` tab.
15. Paste the following:
```
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "VisualEditor0",
                "Effect": "Allow",
                "Action": [
                    "ecr:*",
                    "iam:*",
                    "iot:*",
                    "s3:*",
                    "sagemaker:*"
                ],
                "Resource": "*"
            }
        ]
    }

```
15. Click `Review policy`
16. Give a name to the policy (such as `SmartCameraUserPolicy`) and click
    `Create policy`.

### How to build TRS firmware and root FS

TRS firmware and root FS for supported boards can be built from source as
follows (warning: this requires quite a lot of time and disk space):

```
$ mkdir trs-workspace
$ cd trs-workspace
$ repo init -u https://gitlab.com/Linaro/trusted-reference-stack/trs-manifest.git
$ repo sync -j10
$ make NPROC=$(nproc) TARGET=rockpi4b  # or TARGET=zynqmp-kria-starter
```

Use the `os` target to build the root FS (OS) only, or the `firmware` target to
build the firmware only.

Firmware and root FS images can then be found under the `build` directory:
- Rockpi4b firmware: `build/tmp_rockpi4b/deploy/images/rockpi4b/ts-firmware-rockpi4b.wic.gz`
- Kria KV260 firmware: `build/tmp_zynqmp-kria-starter/deploy/images/zynqmp-kria-starter/ImageA.bin`
- Root FS (common): `build/tmp_trs-qemuarm64/deploy/images/trs-qemuarm64/trs-image-trs-qemuarm64.wic.bz2`

The root FS is a bzip2 file, it should be copied to a USB stick or disk drive with:
```
# bzcat trs-image-trs-qemuarm64.wic.bz2 >/dev/sdX  # Replace X with proper drive letter
```

The KV260 firmware file may be installed as explained above.

The Rockpi4B firmware should be copied to a micro SD card with:
```
# zcat ts-firmware-rockpi4b.wic.gz >/dev/mmcblkX && sync /dev/mmcblkX
```

### How to add layers and packages to TRS

Adding OpenEmbedded/Yocto layers and/or software packages to TRS is not
difficult. For example, here is how to add the `meta-layer` and enable the
`aws-cli`, `greengrass-bin` and `python3-boto3` packages.

```
~/trs-workspace-main/.repo/manifests$ git diff
diff --git a/default.xml b/default.xml
index 240854e..d26eec9 100644
--- a/default.xml
+++ b/default.xml
@@ -29,4 +29,6 @@
         <project path="meta-xilinx"         name="Xilinx/meta-xilinx"          revision="1dfd063eb7f1e98b55468b3de34171f1995bd3f7" upstream="refs/heads/master"         remote="github" />
         <project path="meta-selinux"        name="meta-selinux"                revision="47858343ed2cdc8e39d9e1f916c27db738513dd3" upstream="refs/heads/master"         remote="yocto" />
         <project path="meta-virtualization" name="meta-virtualization"         revision="876a9da42acde2a72ad1e052df4f565d15160e54" upstream="refs/heads/master"         remote="yocto" />
+
+       <project path="meta-aws"            name="aws4embeddedlinux/meta-aws"  revision="b183865d50955d19374401369a2b1330dd2b7901" upstream="refs/heads/master"         remote="github" />
 </manifest>

~/trs-workspace-main/trs$ git diff
diff --git a/meta-trs/conf/templates/default/bblayers.conf.sample b/meta-trs/conf/templates/default/bblayers.conf.sample
index dd3cdd7..cf39c1e 100644
--- a/meta-trs/conf/templates/default/bblayers.conf.sample
+++ b/meta-trs/conf/templates/default/bblayers.conf.sample
@@ -13,6 +13,7 @@ BBLAYERS ?= " \
   ${TOPDIR}/../meta-ledge-secure/meta-ledge-secure \
   ${TOPDIR}/../meta-openembedded/meta-filesystems \
   ${TOPDIR}/../meta-openembedded/meta-initramfs \
+  ${TOPDIR}/../meta-openembedded/meta-multimedia \
   ${TOPDIR}/../meta-openembedded/meta-networking \
   ${TOPDIR}/../meta-openembedded/meta-oe \
   ${TOPDIR}/../meta-openembedded/meta-perl \
@@ -29,4 +30,5 @@ BBLAYERS ?= " \
   ${TOPDIR}/../poky/meta-poky \
   ${TOPDIR}/../meta-ts/meta-trustedsubstrate \
   ${TOPDIR}/../meta-trs \
+  ${TOPDIR}/../meta-aws \
 "
diff --git a/meta-trs/recipes-core/images/trs-image.bb b/meta-trs/recipes-core/images/trs-image.bb
index c6baeb3..367293d 100644
--- a/meta-trs/recipes-core/images/trs-image.bb
+++ b/meta-trs/recipes-core/images/trs-image.bb
@@ -70,6 +70,9 @@ IMAGE_INSTALL += "\
     qemu-system-i386 \
     strace \
     xz \
+    aws-cli \
+    greengrass-bin \
+    python3-boto3 \
 "

 EXTRA_IMAGE_FEATURES += "package-management"

~/trs-workspace-main/meta-ledge-secure$ git diff
meta-ledge-secure$ git diff
diff --git a/meta-ledge-secure/recipes-ledge/images/files/init.ledge b/meta-ledge-secure/recipes-ledge/images/files/init.ledge
index f6cdd7d..f738a81 100644
--- a/meta-ledge-secure/recipes-ledge/images/files/init.ledge
+++ b/meta-ledge-secure/recipes-ledge/images/files/init.ledge
@@ -368,8 +368,8 @@ resize_root_part() {
        # get size on MB
        root_size=$((root_size / (1024 * 1024)))

-       # Avoid resizing if partition is greater than 2GB
-       [ $root_size -gt 2048 ] && return
+       # Avoid resizing if partition is greater than 2.5GB
+       [ $root_size -gt 2560 ] && return

        echo "Resizing $ROOT_DEV part with all space left in $disk"

diff --git a/meta-ledge-secure/wic/ledge-secure.wks.in b/meta-ledge-secure/wic/ledge-secure.wks.in
index 7c44d72..2731d58 100644
--- a/meta-ledge-secure/wic/ledge-secure.wks.in
+++ b/meta-ledge-secure/wic/ledge-secure.wks.in
@@ -2,6 +2,5 @@ bootloader  --ptable gpt --timeout=0  --append="rootwait" --configfile="${GRUB_C
 part /boot --source bootimg-efi --sourceparams="loader=grub-efi" --fstype=vfat --label bootfs --active --align 4 --include-path "${DEPLOY_DIR_IMAGE}/dtb" --uuid 00112233-1234-1111-2222-000123456789 --fsuuid 781974F8 --use-label --part-name="ESP" --part-type 0xef00  --fixed-size 256M

 # root size is required to be fixed in some number to guarantee alignment. If
-# it's changed to 3GB or greater, make sure to update resize_root_part() in
-# init.ledge script
-part / --source rootfs --fstype=ext4 --label rootfs --align 4 --exclude-path boot/ --use-label --fixed-size 2G ${WKS_ROOTFS_PART_EXTRA_ARGS}
+# it's changed, make sure to update resize_root_part() in init.ledge script
+part / --source rootfs --fstype=ext4 --label rootfs --align 4 --exclude-path boot/ --use-label --fixed-size 2560M ${WKS_ROOTFS_PART_EXTRA_ARGS}
```

Note that the size of the root partition is increased from 2G to 2.5G (2560M)
because 2G is not enough to accomodate the additional packages.

After applying the patches, run `make os NPROC=$(nproc)` from the top-level directory.
