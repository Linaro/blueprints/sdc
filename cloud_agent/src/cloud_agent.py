import boto3
import json
import time
from awscrt import io, mqtt, auth, http
from awsiot import mqtt_connection_builder
from src.mqtt_builder import MQTT_builder, on_connection_interrupted
from src.mqtt_builder import on_connection_resumed


class AwsAgent:
    def __init__(self, cloudwatch_post_interval, thing_name):
        self.cloudwatch_post_interval = cloudwatch_post_interval
        self.start_time = time.time()
        self.load_cloud_config()
        self.connect_to_cloud()
        self.topic = self.cloud_config["pub_topic"].replace("THING_NAME",
                                                            thing_name)
        self.sent = False

    def load_cloud_config(self):
        config_file = open("config/service_config.json")
        self.cloud_config = json.load(config_file)

    def connect_to_cloud(self):
        certFolder = self.cloud_config["aws_iot_cert_folder"]
        rootCAPath = certFolder + "/rootCA.pem"
        certificatePath = certFolder + "/thingCert.crt"
        privateKeyPath = certFolder + "/privKey.key"
        host = self.cloud_config["gg_host"]
        port = 8883
        clientId = "device_csp"
        self.conn_builder = MQTT_builder("Basic Connect - Make a MQTT "
                                         "connection.")
        self.conn_handler = self.conn_builder.build_mqtt_connection(
                                on_connection_interrupted,
                                on_connection_resumed, host, port,
                                certificatePath, privateKeyPath, rootCAPath,
                                clientId)
        connect_future = self.conn_handler.connect()
        connect_future.result()
        print("Connected to AWS IOT as MQTT client!")

    def send_metrics_to_cloud(self, metrics):
        metrics = {"person": metrics["person"],
                   "bicycle": metrics["bicycle"], "car": metrics["car"],
                   "bus": metrics["bus"], "truck": metrics["truck"],
                   "End_to_end_FPS": metrics["end_to_end_fps"],
                   "Pure_inf": metrics["pure_fps"],
                   "total_count": metrics["total_count"]}
        if (not self.sent or (time.time() - self.start_time) >
                float(self.cloudwatch_post_interval)):
            metrics["post_cloudwatch"] = "True"
            self.start_time = time.time()
            self.sent = True
        else:
            metrics["post_cloudwatch"] = "False"
        data_str = json.dumps(metrics)
        self.conn_handler.publish(topic=self.topic, payload=data_str,
                                  qos=mqtt.QoS.AT_LEAST_ONCE)
