## Smart Camera Video Capture Container Deployment Component

This AWS component packages the Docker image for the capture container of
the Smart Camera project. Please refer to the main project page at
https://gitlab.com/jforissier/smart-camera-v2/.

## License

The source code in this folder is:

```
Copyright 2023, Linaro Limited
```

and licensed under the terms of:

```
SPDX-License-Identifier: BSD-2-Clause
```
