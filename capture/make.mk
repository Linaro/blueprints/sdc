all: capture-image-save

.PHONY: capture

capture: out/.stamp_capture

capture-image-push: FORCE out/.stamp_capture | $(builder-image)
	$(call run-builder,docker tag sc-capture $(DOCKER_NAMESPACE)/sc-capture)
	$(call run-builder,docker push $(DOCKER_NAMESPACE)/sc-capture)

images-push: capture-image-push

capture-image-save: out/sc-capture.tar.gz

out/.stamp_capture: capture/Dockerfile capture/usb_webcam.yml | host-aarch64-registered $(builder-image)
	$(call run-builder,docker buildx build $(DOCKER_BUILD_OPTS) --platform aarch64 -t sc-capture capture)
	$(q)touch $@

out/sc-capture.tar.gz: out/.stamp_capture | $(builder-image)
	@$(quiet) "  DOCKSAV $@"
	$(q)$(call run-builder,bash -c 'docker save sc-capture | pigz >$@')

staging-gg-cap = out/staging/greengrassv2/SmartCameraCapture

greengrass-capture-component: $(staging-gg-cap)/zip-build/SmartCameraCapture.zip

$(staging-gg-cap)/zip-build/SmartCameraCapture.zip: $(staging-gg-cap)/sc-capture.tar.gz \
				$(staging-gg-cap)/gdk-config.json \
				$(staging-gg-cap)/recipe.yaml | $(builder-image)
	@$(quiet) "  BUILD   $@"
	$(q)$(call run-builder,bash -c 'cd $(staging-gg-cap) && gdk component build')

$(staging-gg-cap)/sc-capture.tar.gz: out/sc-capture.tar.gz
	$(call cp-file)

$(staging-gg-cap)/gdk-config.json: capture/greengrassv2/SmartCameraCapture/gdk-config.json
	$(call cp-file)

$(staging-gg-cap)/recipe.yaml: capture/greengrassv2/SmartCameraCapture/recipe.yaml
	$(call cp-file)

greengrass-capture-component-publish: $(staging-gg-cap)/zip-build/SmartCameraCapture.zip \
				out/.stamp_greengrass-attach-role-policy | $(builder-image)
	@$(quiet) "  PUBLISH $(notdir $<)"
	$(q)$(call run-builder,./aws/gdk_component_publish.sh $(staging-gg-cap))

greengrass-components: greengrass-capture-component

greengrass-components-publish: greengrass-capture-component-publish
