import argparse
import json
import os
import subprocess
import sys
from src.argconfig import parse_args
from src.onnx_to_tvm import onnx_to_tvm

ap = argparse.ArgumentParser()
args = parse_args(ap)

if args["platform"] == "alibaba_cloud":
    with open("ali_config/alibaba_config.json") as json_data_file:
        alibaba_data = json.load(json_data_file)

    print("Authenticating")
    auth = oss2.Auth(alibaba_data["access_key"], alibaba_data["secret_key"])
    print("Connecting to bucket")


def percentage(consumed_bytes, total_bytes):
    if total_bytes:
        rate = int(100 * (float(consumed_bytes) / float(total_bytes)))
        print('\rDownloading Model {0}% '.format(rate), end='')


def download():
    BUCKET_NAME = args["input_bucket"]
    bucket = oss2.Bucket(auth, "http://oss-" + alibaba_data["region"] +
                         ".aliyuncs.com", BUCKET_NAME)
    print("Authorized OSS Bucket ", BUCKET_NAME)
    print("Downloading the fused onnx model...")
    print(args["input_file"])
    bucket.get_object_to_file(args["input_file"], args["input_file"],
                              progress_callback=percentage)


def upload(bucket, file1):
    bucket = oss2.Bucket(auth, "http://oss-"+alibaba_data["region"] +
                         ".aliyuncs.com", bucket)
    bucket.put_object(file1, open(file1, "rb"))
    print("TVM converted Model Uploded to OSS bucket")


if __name__ == "__main__":

    if args["platform"] == "alibaba_cloud":
        download()
        onnx_to_tvm(args["input_file"], args["output_file"])
        upload(args["output_bucket"], args["output_file"])

    elif args["platform"] == "local":
        onnx_to_tvm(args["input_file"], args["output_file"])
