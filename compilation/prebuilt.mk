c := compilation

.PHONY: compilation
compilation: compilation-prebuilt

compilation-prebuilt: $(compiled-model)

$(compiled-model): | $(builder-image)
	@$(quiet) "  MKDIR   $(@D)"
	$(q)mkdir -p $(@D)
	@$(quiet) "  CURL    $@"
	#$(call run-builder,curl -o $@ 'https://gitlab.com/Linaro/blueprints/software-defined-camera/reference-use-cases/traffic-monitoring/application-microservices/cn-inference/-/raw/Release_0.2/models/$(TRAINING_MODEL)/$(TRAINING_MODEL)_$(INFERENCE_ACCELERATOR).tflite?ref_type=heads&inline=false')
	$(call run-builder,curl -o $@ 'https://gitlab.com/Linaro/blueprints/software-defined-camera/reference-use-cases/traffic-monitoring/application-microservices/cn-inference/-/raw/Release_0.2/models/$(TRAINING_MODEL)/$(TRAINING_MODEL)_cpu.tflite?ref_type=heads&inline=false')
	$(call run-builder,curl -o $@ 'https://gitlab.com/Linaro/blueprints/software-defined-camera/reference-use-cases/traffic-monitoring/application-microservices/cn-inference/-/raw/Release_0.2/models/$(TRAINING_MODEL)/$(TRAINING_MODEL)_npu.tflite?ref_type=heads&inline=false')
