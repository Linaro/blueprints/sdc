# Main configuration file

###
### MODEL TRAINING (make, make training)
###

# Where to perform AI model training
#
# off:   no training (TBD: download pre-trained model)
# local: train model on local machine (currently has to be an x86_64 host,
#        preferably with an NVidia GPU)
# aws:   train model in the cloud with AWS. Requires valid AWS credentials
#        (AWS_* below).
#
# The training phase downloads ~20 GB of data from https://cocodataset.org.
# It will use a GPU if available (tested on an x86_64 Amazon EC2 instances
# with NVidia T4 hardware: g4dn.*).

TRAINING_MODE ?= local

# Model type
#
# yolov3:      YOLOv3
# tiny_yolov3: Tiny YOLOv3 (smaller)

TRAINING_MODEL ?= tiny_yolov3

TRAINING_NUMBER_OF_EPOCHS ?= 1500

# The type of AWS instance that should be used to run the training
# Used when TRAINING_MODE == aws only

TRAINING_AWS_INSTANCE_TYPE ?= ml.c4.xlarge

###
### MODEL COMPILATION (make, make compilation)
###

# Compilation mode
#
# off:      model is not compiled
# local:    compile model on local machine
# aws:      compile model with AWS SageMaker
# prebuilt: download prebuilt model from Linaro's GitLab SDC project

COMPILATION_MODE ?= prebuilt

###
### VIDEO CAPTURE
###

# Device to use when running video capture with a camera

VIDEO_DEV ?= /dev/video0

###
### INFERENCE
###

# Accelerator: cpu (no acceleration) or npu (NPU if available)
INFERENCE_ACCELERATOR ?= npu

###
### AWS CREDENTIALS AND CONFIGURATION
###

# In order to avoid accidentally commiting confidential information into the
# source code repository, it is recommended to define the AWS_ variables in
# a local file (aws.mk) rather than to change the values below.
-include aws.mk

# To obtain Amazon Web Services credentials, create an account at
# https://portal.aws.amazon.com/ then in the IAM console create a user to be
# used by this project (for example: SmartCamera)

# Some services may not be available in all regions. Please pick one that
# supports S3 (cloud storage) and SageMaker (Machine Learning).
AWS_REGION ?= eu-west-1

# The AWS_ACCOUNT_ID can be found in various places such as the ARN of the
# SmartCamera user (for example: arn:aws:iam::390323715180:user/SmartCamera).
# It is the numerical part.
AWS_ACCOUNT_ID ?= 390323715180
AWS_ACCESS_KEY_ID ?= AKIAIOSFODNN7EXAMPLE
AWS_SECRET_ACCESS_KEY ?= wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY

AWS_USER_NAME ?= SmartCamera

# The S3 bucket used to store the trained and compiled model
# It will be created if it does not exist
# Note that this name has to be globally unique! (the same name cannot be
# used even in a different AWS account; if it happens to be the case you
# will receive a BucketAlreadyExists error when running "make aws-s3-create").

AWS_S3_BUCKET ?= smartcamerabucket20230223

# Path to the PKCS#11 library on the target system (if supported)
# Used by the 'install-greengrass-pkcs11' command in the sc-demo script.
# libckteec.so.0 is the interface to the OP-TEE PKCS#11 implementation
# Has also been tested with SoftHSM2 (https://github.com/opendnssec/SoftHSMv2)
# with PKCS11_MODULE ?= /usr/local/lib/softhsm/libsofthsm2.so
PKCS11_MODULE ?= /usr/lib/libckteec.so.0

# Assuming proper credentials are configured in $HOME/.docker, Docker images
# can be pushed to the Docker Hub by "make images-push" (all images) or
# "make application-image-push", "make inference-image-push" etc.
# The same images can then be pulled on the device by running:
# "sc-demo install-from-dockerhub" (this does not require any credential unless
# the images are made private, which they are not by default on Docker Hub)
# The Docker Hub is at: https://hub.docker.com/
DOCKER_NAMESPACE ?= invalid

-include conf_local.mk
