#!/usr/bin/env python3

# Note to self: Lots of code here are copy+paste from blueprints-ci.
#               I'm creating a technical debt here instead of creating
#               a shared python lib that can be useful to these 2 projects
#               or even more

from datetime import date
from jinja2 import Environment, FileSystemLoader

import glob
import logging
import os
import re
import requests
import subprocess as sp
import sys


#
#   Set up logging
#
logger = logging.getLogger()
logger.setLevel(os.getenv("DEBUG") and logging.DEBUG or getattr(logging, os.getenv("LOGLEVEL", "INFO")))
ch = logging.StreamHandler()
ch.setFormatter(logging.Formatter("[%(levelname)s] %(message)s"))
logger.addHandler(ch)


SQUAD_BUILD = str(date.today())


INDEX_URL = {
    "trs": {
        "trs-image-trs-qemuarm64.rootfs.wic.bz2": "https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/trs-image-trs-qemuarm64.rootfs.wic.bz2?job=build-meta-trs"
    },
    "rockpi": {
        "ts-firmware-rockpi4b.rootfs.wic.gz": "https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ts-firmware-rockpi4b.rootfs.wic.gz?job=build-meta-ts-rockpi4b",
    },
    "kv260": {
        "ImageA.bin.gz": "https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ImageA.bin.gz?job=build-meta-ts-zynqmp-kria-starter",
        "ImageB.bin.gz": "https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ImageB.bin.gz?job=build-meta-ts-zynqmp-kria-starter",
    },
    "imx93-avh": {
        "Image.signed": "https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/artifacts/Image.signed?job=build-meta-trs",
        "imx93-11x11-evk.dtb": "https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/artifacts/imx93-11x11-evk.dtb?job=build-meta-trs",
    },
    "imx93-evk": {
    },
}


def write_testenv(testenv):
    with open("test.env", "w") as fp:
        for env, value in testenv.items():
            fp.write(f"{env}={value}\n")


def retrieve_real_url(index):
    """
        `INDEX_URL` is a Gitlab's generic URL pointing to the latest successful pipeline.
        We should ping that URL and Gitlab will return a 302 with the actual URL with job id,
        which is the one working in LAVA
    """
    final_urls = {}
    for name, url in INDEX_URL[index].items():
        logger.info(f"Retrieving URL for {name}")
        response = requests.get(url, allow_redirects=False)
        if response.status_code != 302:
            logger.error(response.text)
            return None
        final_urls[name] = response.headers["Location"]
    return final_urls


def run_cmd(cmd, print_stdout=True):
    """
        Run the specified `cmd` and waits for its completion.
        Returns `proc` with extra attributes:
        - `ok` True if the the command returned 0, False otherwise
        - `out` decoded command stdout
        - `err` decoded command stderr
    """
    logger.info(f"Running {cmd}")
    if type(cmd) == str:
        cmd = cmd.split()

    proc = sp.Popen(cmd, stdout=sp.PIPE, stderr=sp.PIPE, bufsize=1, universal_newlines=True)

    stdout = []
    if print_stdout:
        for line in proc.stdout:
            logger.info(line.replace("\n", ""))
            stdout.append(line)

    _, stderr = proc.communicate()
    proc.ok = proc.returncode == 0
    proc.out = "".join(stdout)
    proc.err = stderr
    return proc


#
#   Generate LAVA job definition
#
def generate_lava_job_definition(device, context={}):
    try:
        templates_dir = [".ci/templates/"]
        env = Environment(loader=FileSystemLoader(templates_dir))

        env.globals['basename'] = os.path.basename
        env.globals['splitext'] = os.path.splitext

        return env.get_template(f"devices/{device}.yaml.jinja2").render(context)
    except Exception as e:
        logger.warning(f"Could not generate LAVA job definition: {e}")
        raise e


#
#   Send a job definition to squad, that will send to LAVA
#
def send_testjob_request_to_squad(build, device, job_definition):
    with open("/tmp/definition.yml", "w") as fp:
        fp.write(job_definition)

    cmd = [
        "squad-client",
        "submit-job",
        "--group", os.getenv("SQUAD_GROUP"),
        "--project", os.getenv("SQUAD_PROJECT"),
        "--build", build,
        "--backend", os.getenv("BACKEND"),
        "--environment", device,
        "--definition", "/tmp/definition.yml",
    ]
    proc = run_cmd(cmd)
    if not proc.ok:
        logger.warning(f"Could not submit job: {proc.out}, {proc.err}")
        return False, None

    logger.info(f"See test job progress at {os.getenv('SQUAD_HOST')}/{os.getenv('SQUAD_GROUP')}/{os.getenv('SQUAD_PROJECT')}/build/{build}/testjobs/")

    matches = re.findall(r"SQUAD job id: (\d+)", proc.out + proc.err)
    if len(matches) != 1:
        logger.warning(f"Could not obtain SQUAD job id: match returned {matches}")
        return False, None

    job_id = matches[0]
    return True, job_id


#
#   Find the Gitlab's job id for the job in the next
#   stage after "trigger_tests" job.
#
def find_next_stage_job_id():
    job_name = f"check-{os.getenv('CI_JOB_NAME')}"
    pipeline_jobs_url = f"{os.getenv('CI_API_V4_URL')}/projects/{os.getenv('CI_PROJECT_ID')}/pipelines/{os.getenv('CI_PIPELINE_ID')}/jobs"

    response = requests.get(pipeline_jobs_url)
    if not response.ok:
        logger.warning(f"Could not retrieve id for \"{job_name}\": {response.text}")
        return None

    for job in response.json():
        if job["name"] == job_name:
            return job["id"]

    logger.warning(f"Could not retrieve id for \"{job_name}\" in {response.json()}")
    return None


#
#   Register the next job's URL trigger to SQUAD build
#   meaning that whenever the SQUAD detects that the build finished
#   it'll make an HTTP POST request to the Gitlab job URL, thus triggering
#   the "check" job, which will fetch test results accordingly
#
def register_callback_in_squad():
    job_id = find_next_stage_job_id()
    check_stage_job_url = f"{os.getenv('CI_API_V4_URL')}/projects/{os.getenv('CI_PROJECT_ID')}/jobs/{job_id}/play"

    cmd = [
        "squad-client",
        "register-callback",
        "--group", os.getenv("SQUAD_GROUP"),
        "--project", os.getenv("SQUAD_PROJECT"),
        "--build", SQUAD_BUILD,
        "--url", check_stage_job_url,
        "--record-response",
    ]

    proc = run_cmd(cmd)
    return proc.ok


def trigger_tests():
    test_files = []
    for filename in glob.glob("out/sc-*.tar.gz") + ["out/sc-demo"]:
        test_files.append((filename.replace("out/", "").replace(".tar.gz", ""), os.path.basename(filename)))
    
    device = os.getenv("DEVICE")

    logger.info(f"Test files {test_files}")

    # Generate a nice name to assign jobs in LAVA
    branch_or_tag = os.getenv("CI_COMMIT_BRANCH") or os.getenv("CI_COMMIT_TAG")
    source_slug = f"mr-{os.getenv('CI_MERGE_REQUEST_IID')}" if os.getenv("CI_PIPELINE_SOURCE") == "merge_request_event" else f"branch-{branch_or_tag}"
    job_name = f"{os.getenv('CI_PROJECT_NAME')}-{source_slug}-{os.getenv('CI_JOB_NAME')}-{os.getenv('CI_JOB_ID')}"


    context = {
        "date": SQUAD_BUILD,
        "test_files": test_files,
        "firmware_url": retrieve_real_url(device),
        "os_url": retrieve_real_url("trs"),
        "base_build_url": f"{os.getenv('CI_PROJECT_URL')}/-/jobs/{os.getenv('BUILD_JOB_ID')}/artifacts/raw/out",
        "job_name": job_name,
    }

    logger.info(f"Generating test job definition for {device} and {context}")
    definition = generate_lava_job_definition(device, context=context)
    if definition is None:
        return False

    logger.info(definition)
    logger.info("Submitting test job definition to Squad/LAVA")

    ok, job_id = send_testjob_request_to_squad(SQUAD_BUILD, device, definition)
    if not ok:
        return False

    testenv_out = {
        "SQUAD_JOB_ID": job_id,
    }
    write_testenv(testenv_out)

    logger.info("Registering callback so that next stage is triggered by Squad")
    result = register_callback_in_squad()
    if not result:
        logger.info("Failed to register callback, it probably has been registered in Squad by a previous Gitlab job.")

    logger.info(f"Job has been triggered and is being watched. Once finished, Squad will trigger Gitlab's job \"check-{os.getenv('CI_JOB_NAME')}\", where test results and logs will be present")
    return True


if __name__ == "__main__":
    sys.exit(0 if trigger_tests() else 1)    
