#!/usr/bin/env python3


import sys
import logging
import os
from squad_client.core.api import SquadApi
from squad_client.core.models import TestJob, TestRun
from squad_client.utils import getid


#
#   Set up logging
#
logger = logging.getLogger()
logger.setLevel(os.getenv("DEBUG") and logging.DEBUG or getattr(logging, os.getenv("LOGLEVEL", "INFO")))
ch = logging.StreamHandler()
ch.setFormatter(logging.Formatter("[%(levelname)s] %(message)s"))
logger.addHandler(ch)


#
#   Use settings.SQUAD_JOB_ID to fetch test results from a JOB in LAVA
#   referenced in SQUAD.
#
def fetch_job_from_squad(job_id):

    SquadApi.configure(url=os.getenv('SQUAD_HOST'))

    testjob = TestJob(job_id)
    if not testjob.fetched:
        return testjob

    # Some jobs might fail due to LAVA infra errors. SQUAD detects that and
    # resubmits them automatically. The snippet bellow attemps to find a good
    # resubmitted job
    if testjob.job_status != "Complete":
        resubmitted_jobs = testjob.resubmitted_jobs()
        if len(resubmitted_jobs) > 0:
            logger.info(f"Job {testjob.external_url} had an infrastructure error ({testjob.failure}). SQUAD automatically tried to resubmit it or it was resubmitted manually.")
            good_job = None
            for job_id, job in resubmitted_jobs.items():
                if job.job_status == "Complete":
                    good_job = job
                    break

            if good_job is not None:
                logger.info(f"    Found good resubmitted job {job.external_url}")
                testjob = good_job
            else:
                logger.info("     No good resubmitted job found. This can mean 2 things:")
                logger.info("         a) SQUAD didn't resubmit the offending job at all, or")
                logger.info("         b) All attempts to resubmit the job resulted in failure")

    testjob.tests = []
    if testjob.testrun:
        testrun = TestRun(getid(testjob.testrun))
        tests = testrun.tests(fields="id,name,status").values()
        testjob.tests = sorted(tests, key=lambda t: t.name)

        testjob.log = None

    return testjob


#
#   Check test implementation
#
def check():
    """
        1. Fetch test results for a given SQUAD job id
    """
    right_padding = 50
    bad_test_mark = "<<<<<<<"

    job_id = os.getenv("SQUAD_JOB_ID")
    if job_id is None:
        logger.error("Missing SQUAD_JOB_ID")
        return False

    results = set()
    testjob = fetch_job_from_squad(job_id)
    if not testjob.fetched:
        logger.info("****************************************************************************************************")
        logger.info("")
        logger.info(f"       The LAVA job \"{testjob.external_url}\" ({testjob.environment}) is NOT ready yet!")
        logger.info("       As soon as that LAVA job completes, this Gitlab job will be automatically triggered.")
        logger.info("")
        logger.info("****************************************************************************************************")
        return False

    logger.info(f"******************** Showing details for {testjob.external_url} ({testjob.environment}) ************************")
    logger.info(f"Fetching results from {os.getenv('SQUAD_HOST')}/api/testjobs/{job_id}")

    logger.info(f"Job log: {testjob.external_url}")
    logger.info(f"Job device: {testjob.environment}")
    logger.info("Job timings:")
    logger.info(f"  - created at:   {testjob.created_at}")
    logger.info(f"  - started at:   {testjob.started_at}")
    logger.info(f"  - ended at:     {testjob.ended_at}")
    logger.info(f"  - fetched at:   {testjob.fetched_at}")

    if testjob.job_status != "Complete":
        logger.warning(f"Job failure: {testjob.failure}")

        # Some devices might fail due to unexpected infrastructure error
        # and these should have "bypass_failure" enabled in the device test plan
        device_plan = get_test_plan()[testjob.environment]
        if device_plan.get("bypass_failure"):
            logger.info(f"{testjob.environment} is configured not to fail CI even if job didn't finish correctly")
        else:
            results.add("fail")

    elif len(testjob.tests) == 0:
        logger.info("Job has no tests")
        results.add("fail")
    else:
        logger.info("Job tests:")
        for test in testjob.tests:
            logger.info(f"  - {test.name:<{right_padding}}: {test.status} {bad_test_mark if test.status == 'fail' else ''}")
            results.add(test.status)

    logger.info(f"******************** End of details for {testjob.external_url} ({testjob.environment}) ************************")

    return "fail" not in results


if __name__ == "__main__":
    sys.exit(0 if check() else 1)
