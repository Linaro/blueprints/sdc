$(shell mkdir -p out ~/.cache/ccache ~/.docker)

all:

export

.DELETE_ON_ERROR:

include conf.mk

include helpers.mk

clean:
	$(call rm-r,out)
	$(q)find . -name __pycache__ | while read f; do $(quiet) "  RM-R    $$f"; rm -rf $$f; done
	$(call rm,application/greengrassv2/SmartCameraApplication/gdk-config.json)
	$(call rm,aws/S3BucketPermissions.json)
	$(call rm,aws/SmartCameraGreengrassPolicy.json)
	$(call rm,capture/greengrassv2/SmartCameraCapture/gdk-config.json)
	$(call rm,capture/usb_webcam.yml)
	$(call rm,compilation/aws/sagemaker_config.json)
	$(call rm,inference/greengrassv2/SmartCameraInference/gdk-config.json)
	$(call rm,sc-demo)
	$(call rm,training/aws/ECRRepositoryPolicy.json)
	$(call rm,training/aws/entrypoint)
	$(call rm,cloud_agent/config/service_config.json)
	$(call rm,cloudwatch/greengrassv2/SmartCameraCloudWatch/gdk-config.json)
	$(call rm,cloudwatch/lambda.py)
	$(call rm,test/greengrassv2/SmartCameraTest/gdk-config.json)
	$(call rm,aws/GreengrassDeployment.json)

builder-image := out/.stamp_builder-image
builder-image: $(builder-image)

# DOCKER_BUILD_OPTS=--no-cache may be useful in case of 404 error
DOCKER_BUILD_OPTS ?= --no-cache
$(builder-image): builder/Dockerfile
	@$(quiet) "  DOCKBLD sc-builder"
	$(q)docker build $(DOCKER_BUILD_OPTS) \
		--build-arg HOST_DOCKER_GID=$$(stat -c %g /var/run/docker.sock 2>/dev/null || echo 0) \
		--build-arg USER_ID=$$(id -u) \
		--build-arg USER_GID=$$(id -g) \
		--build-arg HOST_ARCH=$$(uname -m) \
		--build-arg AWS_REGION=$(AWS_REGION) \
		--build-arg AWS_ACCESS_KEY_ID=$(AWS_ACCESS_KEY_ID) \
		--build-arg AWS_SECRET_ACCESS_KEY=$(AWS_SECRET_ACCESS_KEY) \
		-t sc-builder builder
	$(q)touch $@

builder-image-clean:
	$(call rm,$(builder-image))

clean: builder-image-clean

builder-image-run: $(builder-image)
	$(call run-builder-nofilter,,,-it)

builder-image-run-root: $(builder-image)
	$(call run-builder-nofilter,,,-u 0:0 -it)

rootfs-trs := out/trs-image-trs-qemuarm64.rootfs.wic.bz2
rootfs-trs-url := https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/trs-image-trs-qemuarm64.rootfs.wic.bz2?job=build-meta-trs

$(rootfs-trs): | $(builder-image)
	@$(quiet) "  CURL    $@"
	$(call run-builder,curl --create-dirs -L -o $@ $(rootfs-trs-url))

# Partition number 2 is e.g., /dev/mmcblk0p2 or /dev/sdb2
PART := $(if $(findstring mmcblk,$(DEV)),$(DEV)p2,$(DEV)2)

trs-rootfs-flash:
	@if [ ! "$(DEV)" ]; then \
		echo 'Usage: make $@ DEV=<device path> (such as /dev/sdb)'; \
		false; \
	fi
	sudo sh -c 'bzcat $(rootfs-trs) >"$(DEV)"'
	sudo sync "$(DEV)"

include aws/make.mk
include training/make.mk
include compilation/make.mk
include inference/make.mk
include capture/make.mk
include application/make.mk
ifeq (,$(findstring EXAMPLE,$(AWS_ACCESS_KEY_ID)))
HAS_CLOUD_AGENT := true
include cloud_agent/make.mk
include cloudwatch/make.mk
else
HAS_CLOUD_AGENT := false
endif
include test/make.mk

include rockpi4/make.mk
include kv260/make.mk
include imx93/make.mk
include corstone1000-asvck/make.mk

all: sc-demo

deploy-files := inference/traffic.mp4 out/sc-application.tar.gz out/sc-capture.tar.gz out/sc-inference.tar.gz out/sc-test.tar.gz
ifeq (true,$(HAS_CLOUD_AGENT))
deploy-files += out/sc-cloud_agent.tar.gz
endif
ifeq (,$(K3S))
deploy-files += sc-demo
else
deploy-files += sc-demo-k3s sc-pod-ca.yaml sc-pod-noca.yaml
endif
deploy: $(deploy-files)
	@if [ ! "$(TO)" ]; then \
		echo 'Usage: make $@ TO=<board IP or DNS name>'; \
		false; \
	fi
	$(call scp,$(deploy-files),$(TO))
