# Makefile helpers

# Definitions to support make / make V=1 / make -s
# - With 'make', full commands may not be displayed, instead they are replaced
#   by a short line
# - With 'make V=1" all commands are displayed
# - With 'make -s' no output is produced except on error
ifneq ($V,1)
q := @
quiet := echo
else
q :=
quiet := true
endif
ifneq ($(filter 4.%,$(MAKE_VERSION)),)  # make-4
ifneq ($(filter %s ,$(firstword x$(MAKEFLAGS))),)
quiet := true
endif
else                                    # make-3.8x
ifneq ($(findstring s, $(MAKEFLAGS)),)
quiet := true
endif
endif

process-in-file = awk -f subst.awk $(1) $(2)

%: %.in conf.mk
	@$(quiet) "  GEN    $@"
	$(q)$(call process-in-file,$<,>$@)

%.tar.gz: %
	@$(quiet) "  TAR-C  $@"
	$(q)tar czf $@ -C $(dir $<) $(notdir $<)

#
# Allow building aarch64 Docker images on x86_64 host
#

host-arch := $(shell uname -m)

ifneq (,$(filter-out x86_64 aarch64,$(host-arch)))
$(error Unsupported host architecture: $(host-arch))
endif

# would return errors such as: "exec /bin/sh: exec format error".
#
# Note that this step registers binfmt_misc QEMU helpers for architectures
# different from the host, so that foreign arch images can be built and run
# See https://github.com/multiarch/qemu-user-static
ifeq ($(host-arch),x86_64)
host-aarch64-registered: /proc/sys/fs/binfmt_misc/qemu-aarch64

/proc/sys/fs/binfmt_misc/qemu-aarch64:
	@$(quiet) "  DOCKRUN multiarch/qemu-user-static"
	$(q)docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
else
host-aarch64-registered:
endif

# Source (project) directory is mounted under /smart-camera in the container
# It must be possible to override the value to cater for Docker-in-Docker (CI):
# the path has to be the host path (where the Docker daemon runs)
SRCDIR ?= $(CURDIR)
CCACHE_DIR_DOCKER ?= $(shell echo $${CCACHE_DIR-$$HOME/.cache/ccache})
# $(call run-builder,<args>[[,<image name>],<docker args>])
define run-builder
	@$(quiet) "  DOCKRUN $(if $(2),$(2),sc-builder) [$(1)]" | sed 's@$(AWS_SECRET_ACCESS_KEY)@<REDACTED>@g'
	$(q)_status=0; (docker run --privileged --network host $(3) --rm \
		-v $(SRCDIR):/smart-camera \
		-v $(CCACHE_DIR_DOCKER):/home/sc-user/.cache/ccache \
		-v $(CCACHE_DIR_DOCKER):/home/sc-user/.buildroot-ccache \
		-v /var/run/docker.sock:/var/run/docker.sock \
		-v /dev:/dev \
		-v $${HOME}/.docker:/home/sc-user/.docker \
		$(if $(2),$(2),sc-builder) $(1) 2>&1 || export _status=$$?) | sed 's@$(AWS_SECRET_ACCESS_KEY)@<REDACTED>@g'; (exit $${_status})
endef
define run-builder-nofilter
	@$(quiet) "  DOCKRUN $(if $(2),$(2),sc-builder) [$(1)]" | sed 's@$(AWS_SECRET_ACCESS_KEY)@<REDACTED>@g'
	$(q)docker run --privileged --network host $(3) --rm \
		-v $(SRCDIR):/smart-camera \
		-v $(CCACHE_DIR_DOCKER):/home/sc-user/.cache/ccache \
		-v $(CCACHE_DIR_DOCKER):/home/sc-user/.buildroot-ccache \
		-v /var/run/docker.sock:/var/run/docker.sock \
		-v /dev:/dev \
		-v $${HOME}/.docker:/home/sc-user/.docker \
		$(if $(2),$(2),sc-builder) $(1)
endef

make-in-builder = $(call run-builder,$(MAKE) SRCDIR=$(CURDIR) $(1) $(if $(V),V=$(V),),$(2))

define s3-upload-if-not-exists
	@$(quiet) "  CHECK   s3://$(AWS_S3_BUCKET)/$(notdir $(1))"
	$(q)aws s3 ls s3://$(AWS_S3_BUCKET)/$(notdir $(1)) >/dev/null || { \
		$(quiet) "  S3CP    s3://$(AWS_S3_BUCKET)/$(notdir $(1))"; \
		aws s3 cp $(1) s3://$(AWS_S3_BUCKET); }
endef

define s3-download
	@$(quiet) "  S3CP    $(2)"
	$(q)aws s3 cp s3://$(AWS_S3_BUCKET)/$(1) $(2)
endef

define s3-rm
	@$(quiet) "  S3RM    s3://$(AWS_S3_BUCKET)/$(1)"
	$(q)aws s3 rm s3://$(AWS_S3_BUCKET)/$(1)
endef

define cp
	@$(quiet) "  CP      $2/$(notdir $(1))"
	$(q)cp $1 $2
endef

define cp-r
	@$(quiet) "  CP-R    $2/$(notdir $(1))"
	$(q)cp -R $1 $2
endef

define rm
	@$(quiet) "  RM      $1"
	$(q)rm -f $1
endef

rmsh := $(quiet) "  RM      $1"; rm -f $1

define rmdir
	@$(quiet) "  RMDIR   $1"
	$(q)rmdir $1
endef

define rm-r
	@$(quiet) "  RM-R    $1"
	$(q)rm -rf $1
endef

define mkdir
	@$(quiet) "  MKDIR   $1"
	$(q)mkdir -p $1
endef

define cp-file
        @$(quiet) "  CP      $@"
	$(q)mkdir -p $(dir $@)
        $(q)cp $< $@
endef

define scp
	@$(quiet) "  SCP     $1 root@$2:"
	$(q)scp $(SCP_ARGS) $1 root@$2:
endef

# Rename $1 to $2 only if file content differs. Otherwise just delete $1.
define mv-if-changed
	if cmp -s $2 $1; then			\
		rm -f $1;			\
	else					\
		$(quiet) '  UPD     $2';	\
		mv $1 $2;			\
	fi
endef

define download
	@$(quiet) "  CURL     $2"
	$(q)curl -s -o $2 $1
endef

compiled-model := out/$(TRAINING_MODEL)/models/$(TRAINING_MODEL)_$(INFERENCE_ACCELERATOR).tflite

.PHONY: FORCE
FORCE:

# append-br2-vars():
# All $(2)BR2_* variables from the makefile or the environment are appended to
# $(1). All values are quoted "..." except y and n.
# $(2) is a prefix
double-quote = "#" # This really sets the variable to " and avoids upsetting vim's syntax highlighting
streq = $(and $(findstring $(1),$(2)),$(findstring $(2),$(1)))
y-or-n = $(or $(call streq,y,$(1)),$(call streq,n,$(1)))
append-var_ = echo '$(subst $(4),,$(1))=$(3)'$($(1))'$(3)' >>$(2);
append-var = $(call append-var_,$(1),$(2),$(if $(call y-or-n,$($(1))),,$(double-quote)),$(3))
append-br2-vars = $(foreach var,$(filter $(2)BR2_%,$(.VARIABLES)),$(call append-var,$(var),$(1),$(2)))
