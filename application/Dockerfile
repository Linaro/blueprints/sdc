FROM python:3.7-slim-buster as initial
ARG DEBIAN_FRONTEND=noninteractive
RUN apt update -y && apt install -y \
	gcc \
	libgl1-mesa-glx \
	libglib2.0-0

RUN python3 -m pip install --upgrade pip && python3 -m pip install \
	boto3 \
	backports.zoneinfo \
	flask \
	flask_ngrok \
	grpcio \
	grpcio-tools \
	opencv-python \
	requests

RUN apt remove -y \
	gcc
RUN apt autoremove -y && rm -rf /var/lib/apt/lists/*
RUN rm -rf /root/.cache

COPY static/ static/
COPY model_data/ model_data/
COPY config config/
COPY templates/ templates/
COPY src src/
COPY classes classes/

WORKDIR src/grpc_files
CMD python gen.py

WORKDIR /
COPY main.py main.py

FROM scratch
COPY --from=initial / /
ENTRYPOINT ["python3","main.py"]
