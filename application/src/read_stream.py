"""
Copyright © 2024 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
import time
import cv2
from threading import Thread
from src.logger import get_logger

# Create logger for various debugging logs.
logger = get_logger()

class ReadFromRTSP(Thread):
    """
    Thread class for reading frames from an RTSP stream.

    Args:
        rtsp_src (str): RTSP stream URL.

    Raises:
        Exception: If the RTSP stream cannot be initialized.
    """

    def __init__(self, rtsp_src):
        Thread.__init__(self)
        self.stream = cv2.VideoCapture(rtsp_src)

        if not self.stream.isOpened():
            logger.warning("Could not intitialize the RTSP stream.....")
            raise Exception("RTSPStreamInitializationError")

        self.stream.set(cv2.CAP_PROP_BUFFERSIZE, 1)
        self.status, self.frame = self.stream.read()
        while not self.status:
            self.status, self.frame = self.stream.read()
        self.stop = False
        self.frame_read_time = 0

    def stop(self):
        self.stop = True

    def run(self):
        while (self.stream.isOpened()):
            self.frame_read_start = time.time()
            self.status, self.frame = self.stream.read()
            if self.frame is None:
                continue
            frame_read_end = time.time()
            self.frame_read_time = frame_read_end - self.frame_read_start
            if (self.frame is None) or (self.stop == True):
                logger.info("Stream stopped / Connection broken.......")
                break

    def read(self):
        return self.status, self.frame

    def isOpened(self):
        return self.stream.isOpened()

    def get(self, id):
        return self.stream.get(id)


class ReadFromRTMP(Thread):
    """
    Thread class for reading frames from an RTMP stream.

    Args:
        rtmp_src (str): RTMP stream URL.
    """

    def __init__(self, rtmp_src):
        Thread.__init__(self)
        self.rtmp_src = rtmp_src
        self.stop = False
        self.status = False
        self.frame = None

    def connect_stream(self):
        time.sleep(20)
        try:
            self.stream = cv2.VideoCapture(self.rtmp_src)
        except cv2.error as e:
            logger.info("Connecting to RTMP....")

        while not self.stream.isOpened():
            try:
                self.stream = cv2.VideoCapture(self.rtmp_src)
            except cv2.error as e:
                logger.info("Reconnecting......")

        if not self.stream.isOpened():
            logger.warning("Could not intitialize the RTMP stream.....")

        self.status, self.frame = self.stream.read()
        while not self.status:
            self.stream = cv2.VideoCapture(self.rtmp_src)
            self.status, self.frame = self.stream.read()
            logger.info("getting the frame....")

    def run(self):
        self.connect_stream()
        while (True):
            self.status, self.frame = self.stream.read()
            if (self.frame is None) or (self.stop == True):
                self.connect_stream()
                raise Exception("Stream stopped / Connection broken")

    def read(self):
        return self.status, self.frame

    def isOpened(self):
        return self.stream.isOpened()
