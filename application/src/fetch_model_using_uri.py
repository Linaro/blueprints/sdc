"""
Copyright © 2024 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
import boto3
import os
import json
from src.logger import get_logger
from botocore.exceptions import NoCredentialsError, PartialCredentialsError, ClientError

logger = get_logger()

s3 = boto3.client('s3')

with open("config/aws_credential.json",'r') as config_file:

    aws_data = json.load(config_file)
    if aws_data is None:
        logger.warning("AWS config file is not available or invalid.")
        raise ValueError("AWS config file is not available or invalid.")


def download_model_files_s3(s3_uri, local_dir, file_to_check):

    """
    Download model files from s3 bucket.

    This function downloads the custom model folder from AWS s3 bucket into local directory on device to
    provide custom model support in SDC.

    Args:
        s3_uri : Link to folder in the s3 bucket.
        local_dir : Path to which files to be downloaded from s3 bucket.
        file_to_check : File name to download from s3 bucket.

    """

    s3 = boto3.client('s3',
                aws_access_key_id=aws_data["accessKey"],
                aws_secret_access_key=aws_data["secretKey"],
                region_name=aws_data["region"]
                )

    if not s3_uri.startswith('s3://'):
        raise ValueError('Invalid S3 URI. It should start with "s3://".')

    s3_uri_parts = s3_uri[len('s3://'):].split('/', 1)
    bucket_name = s3_uri_parts[0]
    s3_key = s3_uri_parts[1]

    object_key = os.path.join(s3_key, file_to_check)

    if not os.path.exists(local_dir):
        os.makedirs(local_dir)

    try:
        s3.head_object(Bucket=bucket_name, Key=object_key)
        logger.info(f"Object {object_key} exists. Proceeding to download.")

        if not os.path.exists(local_dir):
            os.makedirs(local_dir)
        elif not os.path.isdir(local_dir):
            raise NotADirectoryError(f"Expected '{local_dir}' to be a directory, but it is a file.")

        local_file_path = os.path.join(local_dir, os.path.basename(object_key))

        s3.download_file(bucket_name, object_key, local_file_path)
        logger.info(f"File {object_key} downloaded successfully to {local_file_path}")

    except ClientError as e:
        if e.response['Error']['Code'] == '404':
            logger.error(f"Error: The specified object '{object_key}' does not exist in the bucket.")

        else:
            logger.error(f"Error: {e}")

    except NoCredentialsError:
        logger.error("Error: No AWS credentials found.")

    except PartialCredentialsError:
        logger.error("Error: Incomplete AWS credentials.")

    except Exception as e:
        logger.error(f"Unexpected error: {e}")
