"""
Copyright © 2024 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

import cv2,random

def generate_unique_rgb_colors(num_colors):

    """Generates a list of unique RGB colors
        Args:
            num_colors (str): length of class_ids according to model.

        Returns:
            list: List of random colors.
    """
    colors = set()
    while len(colors) < num_colors:
        color = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
        colors.add(color)
    return list(colors)

def assign_colors_to_class_ids(class_ids):

    """Assigns unique RGB colors to each class ID
    Args:
            class_ids (str): class_ids according to model.

        Returns:
            dict : dictionary of randomly generated colors and class_ids .
    """
    num_classes = len(class_ids)
    unique_colors = generate_unique_rgb_colors(num_classes)
    color_dict = {class_id: unique_colors[i] for i, class_id in enumerate(class_ids)}
    return color_dict

def draw_bb(right_boxes, right_classes, right_scores, frame,color_dict,label_dict,label_name):
    """
    Draw bounding boxes and labels on an image.

    Args:
        frame (numpy.ndarray): The image on which bounding boxes and labels will be drawn.
        right_boxes (list): List of bounding boxes.
        right_scores (list): Confidence scores for each detected object.
        right_classes (list): List of class labels for each detected object.
        color_dict : dictionary of randomly generated colors and class_ids .
        label_name : List of class names.
        label_dict : dictionary of label names according to desired class_ids.


    Returns:
        numpy.ndarray: The input image with bounding boxes and labels drawn on it.
        list : The array which contain count of each detected classes according to model.
    """

    if len(right_classes) == 0:

        final_detection = label_dict.copy()
        print(final_detection)
        return final_detection, frame

    final_detection = label_dict.copy()

    for i in range(len(right_boxes)):
        class_label = int(right_classes[i])
        # JF
        if class_label not in color_dict.keys():
            break
        box = right_boxes[i]
        x0 = int(box[1] * frame.shape[1])
        y0 = int(box[0] * frame.shape[0])
        x1 = int(box[3] * frame.shape[1])
        y1 = int(box[2] * frame.shape[0])
        cv2.rectangle(frame, (x0, y0), (x1, y1), color_dict[int(class_label)], 2)
        cv2.rectangle(frame, (x0, y0), (x0 + 100, y0 - 15), color_dict[(int(class_label))], -1)
        cv2.putText(frame,str(label_name[class_label]) + ",  " + str(int(right_scores[i] *100)) + "%",( x0, y0),cv2.FONT_HERSHEY_SIMPLEX,0.5,(255, 255, 255),2)
        final_detection[label_name[int(class_label)]] +=1
    return final_detection, frame

def classification(labels,right_classes):

    """Map right_classes indices to their corresponding labels and log the result.
    Args:
            labels : List of class names.
            right_classes (list): List of class labels for each detected object.

        Returns:
            class_name: List of class_names on detected object.
    """
    class_name = [labels[i] for i in right_classes]
    return class_name

def decode_class_names(classes_path):

        """
        Decode class names from a file.

        Args:
            classes_path (str): Path to the file containing class names, one per line.

        Returns:
            list: List of class names.
        """

        with open(classes_path, 'r') as f:
            lines = f.readlines()
        classes = []
        for line in lines:
            line = line.strip()
            if line:
                classes.append(line)
        return classes
