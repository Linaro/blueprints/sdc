"""
Copyright © 2024 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
import cv2
from src.read_stream import ReadFromRTSP
from src.logger import get_logger

# Create logger for various debugging logs.
logger = get_logger()

def get_camera_object(args):
    """
    Get a camera object based on the input arguments.

    This function initializes and returns a camera object either from a local USB camera or an RTSP stream.

    Args:
        args (dict): Input arguments containing camera information.

    Returns:
        camera: A camera object, either cv2.VideoCapture for a local USB camera or ReadFromRTSP for an RTSP stream.
    """

    try:
        # Check if the input argument specifies an RTSP stream or a local USB camera
        if args["rtsp"] == "":
            logger.info("Taking input from local USB camera....")
            # Initialize a camera object for USB camera
            camera = cv2.VideoCapture(args["cam_path"])
        else:
            logger.info("Taking input from RTSP stream....")
            # Initialize a camera object for RTSP stream
            camera = ReadFromRTSP(args["rtsp"])
            camera.start()  # Start the RTSP stream reader thread
    except Exception as e:
        logger.warning("Could not intitialize the Camera....")
        raise Exception("CameraInitializationError")

    return camera
