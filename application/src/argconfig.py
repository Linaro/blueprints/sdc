"""
Copyright © 2024 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
def ArgConf(ap):
    """
     Configure and parse command line arguments.

     This function configures and parses command line arguments using the argparse module.

     Args:
         ap (argparse.ArgumentParser): An ArgumentParser object for adding and parsing arguments.

     Returns:
         dict: A dictionary containing the parsed command line arguments.

     """
    ap.add_argument("-ip", "--ipaddr", default="127.0.0.1",
                    help="ip address the device where inference container is running")
    ap.add_argument("-csp", "--cloud_address", default="",
                    help="Cloud container ip and port number eg. 127.0.0.1:8081")
    ap.add_argument("-csp_s", "--cloud_rtmp_stream", default="",
                    help="RTMP pull url for fetching the stream")
    ap.add_argument("-p", "--inf_port", default="8080",
                    help="inference container port number")
    ap.add_argument("-c", "--cam_path", help="Camera device Path")
    ap.add_argument("-r", "--rtsp", default="", help="RTSP stream link")
    ap.add_argument("-nui", "--disable_ui", action="store_false",
                    help="Disable Web UI integration")
    ap.add_argument("-cloud", "--cloud_service", help="aws/alibaba")
    ap.add_argument("-lv","--linkvisual",default="",help="on")
    ap.add_argument("-s","--size",default="416",help="resizing image")
    ap.add_argument("-m", "--model", required=True, help="Model to be used (tiny_yolov3/yolov3/mssd_v1/mssd_v2/mobilenetv2)")
    ap.add_argument("-str","--webui_stream",default="local",help="Taking frames for streaming from either KVS or local")
    ap.add_argument("-uri","--uri",help="link to download the model provided by the user")
    ap.add_argument("-a", "--accelerator", required=True, default="cpu", help="Accelerator to be used (cpu/npu)")
    args = vars(ap.parse_args())

    return args
