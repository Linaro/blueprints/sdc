"""
Copyright © 2024 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
import json
import cv2
import grpc
from src.grpc_files import Datas_pb2
from src.grpc_files import Datas_pb2_grpc

def Request(frame):
    """ Generate grpc request with the encoded image data. """
    ret, buf = cv2.imencode('.jpg', frame)
    if ret != 1:
        return
    yield Datas_pb2.Request(datas=buf.tobytes())

def start_grpc_connection(inf_ip, inf_port):
    """ Start a grpc connection to the inference server."""
    try:
        channel = grpc.insecure_channel(inf_ip + ":" + inf_port)
        stub = Datas_pb2_grpc.MainServerStub(channel)
        return stub
    except grpc.RpcError as e:
        raise Exception("Failed to establish gRPC connection to the inference server. Error: {}".format(e.details()))

def send_for_inference(img, stub):
    """
    Send image for inference to a gRPC server.

    Args:
        img : Input image for inference.
        stub (grpc._Channel): gRPC stub for communication.

    Returns:
        tuple: A tuple containing inference results - right_boxes, right_classes, right_scores,
               model_load_time, and pure_inference_time.

    """
    resp = stub.getStream(Request(img))
    for i in resp:
        res = i.reply
        data = json.loads(res)

        right_boxes = data["right_boxes"]
        right_classes = data["right_classes"]
        right_scores = data["right_scores"]
        model_load_time = data["model_load"]

        pure_inference_time = float("{:.3f}".format(float(data["inf_time"])))

    return right_boxes,right_classes,right_scores,model_load_time, pure_inference_time
