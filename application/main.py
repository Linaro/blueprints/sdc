"""
Copyright © 2024 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

import argparse
import json
import time
import requests
import flask
import cv2
from flask import Flask, render_template, Response, redirect 
from src.read_stream import ReadFromRTMP 
from src.argconfig import ArgConf
from src.camera import get_camera_object
from src.inference import send_for_inference, start_grpc_connection
from src.logger import get_logger
from src.img_helper import draw_bb,assign_colors_to_class_ids,classification, decode_class_names
from src.fetch_model_using_uri import download_model_files_s3
from src.const import *

ap = argparse.ArgumentParser()
args = ArgConf(ap)

app = Flask(__name__)

logger = get_logger()

global model_info, model_type

camera = get_camera_object(args)
if not camera.isOpened():
    error_message = "Could not open camera. Please check camera or rtsp path"
    logger.error(error_message)
    camera.release()
    raise Exception(error_message)

cloud_stream = None
if args["cloud_rtmp_stream"] != "":
    cloud_stream = ReadFromRTMP(args["cloud_rtmp_stream"])
    cloud_stream.start()
    
kvs_config_file = open("config/kvs_credential.json")
kvs_new_config = json.load(kvs_config_file)

#Loading files
model_detail_file_path = "config/model_detail.json"

if args["model"] == "custom":
    if args["uri"] == "local":
        model_detail_file_path = "custom_model/model_detail.json"
    else:
        download_path = './custom_model/'
        model_detail_file_path = "custom_model/model_detail.json"
        download_model_files_s3(args["uri"], download_path, "model_detail.json")
        download_model_files_s3(args["uri"], download_path, "classes.txt")

logger.info("Taking model_details from:%s", model_detail_file_path)


with open(model_detail_file_path,'r') as m:
    model_info = json.load(m)
    logger.info("Loaded model detail config file.")

classes_2 = []
classification_scores = []
acc=""
acc = args["accelerator"]
size = int(args["size"])
model = args["model"]
logger.info(f"Model name- {model}")

"""get accelerator data"""
if args["accelerator"] == "cpu":
    acc="Cortex-A55 CPU"
elif args["accelerator"] == "npu":
    acc="Ethos-U NPU"

"""Get the model details for the specified accelerator and model name"""
model_infor = model_info.get(acc, {}).get(model, {})
model_type = model_info.get(acc, {}).get(model, {}).get("model_type")

class_name_path = model_info.get(acc, {}).get(model, {}).get("class_name_path")
desired_class_ids = model_info.get(acc, {}).get(model, {}).get("desired_class_ids")
model_type = model_info.get(acc, {}).get(model, {}).get("model_type")

label_name = decode_class_names(class_name_path)  

frame_count = 0
frame_read_time_avg = 0
preprocess_time_avg = 0
inference_time_avg = 0
postprocess_time_avg = 0
cloud_metrics_time_avg = 0
pure_inf_time_avg = 0
rest_inf_time_avg = 0
normal_cam_read = 0
pipeline_time_avg = 0
pure_inf_time = 0
right_boxes = 0
web_ui_fps = 0
web_ui_fps_avg = 0
web_ui_time = 0
frame_num = 0
cloud_metrics_time=0

arr = {}
rest_inf_time = 0
pure_inference_time = 0
total_count = 0
   
def on_message_received(topic, payload, dup, qos, retain, **kwargs):
    """
    Callback function to handle incoming MQTT messages.

    This function is triggered when a message is received on a subscribed MQTT topic. It processes the payload, 
    extracts relevant information from the JSON message, and updates global variables accordingly.
    """
    global person, truck, bicycle, bus, car, total_count, pure_inference_time, rest_inf_time

    logger.info("Received message from topic '%s': %s", topic, payload)
    payload_str = payload.decode("utf-8")
    logger.info("Received payload type: %s", type(payload_str))                                                                                                                                                       
    command = str(payload.decode("utf-8"))  
    jsonMsg  = json.loads(command)    
    logger.info("Received JSON message type: %s", type(jsonMsg))
    logger.info("Received command: %s", jsonMsg)

    person = int(jsonMsg["person"])
    car = int(jsonMsg["car"])
    bicycle = int(jsonMsg["bicycle"])
    truck = int(jsonMsg["truck"])
    bus = int(jsonMsg["bus"])
    total_count = int(jsonMsg["total_count"])
    pure_inference_time = float(jsonMsg["Pure_inf"])
    rest_inf_time = float(jsonMsg["End_to_end_FPS"])

def no_ui_inference():
    """
    Perform inference on video frames without displaying a UI.

    This function reads video frames, processes them through an inference pipeline,
    and provides various metrics and results without a graphical user interface.

    Returns:
        None
    """
    global arr, model_data, frame_num, right_classes, right_scores, right_boxes, rest_inf_time, pure_inference_time, total_count, pipeline_time, acc, model_data,size
    global pipeline_time_avg, normal_cam_read, frame_read_time_avg, preprocess_time_avg, inference_time_avg, postprocess_time_avg, cloud_metrics_time_avg, frame_count, pure_inf_time_avg, rest_inf_time_avg, web_ui_time, web_ui_time_avg
    global label_name

    if model_type != "classification" :
    
        color_dict = assign_colors_to_class_ids(desired_class_ids)
        label_dict = {label_name[int(class_ids)]: 0 for class_ids in desired_class_ids} 
    else :
        color_dict = None
        label_dict = None
        
    while(camera.isOpened()):
        logger.info(f"--------------------------Frame no. - {frame_num}-------------------------------")
        #Reading Frames
        start = time.time()
        ret, frame = camera.read()  
        normal_cam_read += time.time() - start    
        frame_read_time = camera.frame_read_time
        if frame is None:
            logger.error('End of video/video not detected')
            exit()
        
        #preprocess
        prepro_start_time = time.time()
        img = cv2.resize(frame, (size, size))
        preprocess_time = time.time() - prepro_start_time
        
        #GRPC start
        inference_conn = start_grpc_connection(args["ipaddr"], args["inf_port"])
        
        #inferencing
        if model_type == "classification" :
        
            #sending frames for inferencing
            global classes_2, classification_scores,arr
            inf_start = time.time()
            right_boxes, right_classes, right_scores, model_load_time, pure_inference_time = send_for_inference(img, inference_conn)
            inference_time = time.time() - inf_start
            logger.info("Inference time--------%s",pure_inference_time)
            postpro_start = time.time()
            classes_2 = classification(label_name,right_classes)

            arr = dict(zip(classes_2,right_scores))
        else:
            #sending frames for inferencing
            inf_start = time.time()
            right_boxes,right_classes,right_scores,model_load_time, pure_inference_time = send_for_inference(img, inference_conn)
            inference_time = time.time() - inf_start
            postpro_start = time.time()
            #drawing bounding boxes
            arr, img = draw_bb(right_boxes, right_classes, right_scores,frame,color_dict,label_dict,label_name)

        total_count=sum(arr.values())
        postprocess_time = time.time() - postpro_start
        logger.info("Application-side Preprocess time: %s", round(preprocess_time,4))
        logger.info("Application-side Postprocess time: %s", round(postprocess_time,4))

        #sending data to cloud container
        if model_type != "classification" and args["cloud_address"] != "" :
        
            cloud_info_start = time.time()
            image_string = cv2.imencode('.jpg', img)[1]
            image_tensor = image_string.tolist()
            
            iot_resp = {"cloud_metrics":{"final_detection": arr,
                            "pure_fps": pure_inference_time, "end_to_end_fps": rest_inf_time, "total_count": total_count}, 
                            "image": image_tensor,
                            "app_time": {"Start_time": 0, "Cloud_start": 0}}
            
            resp = requests.post("http://"+ args["cloud_address"] +"/video",json=iot_resp)
            resp = json.loads(resp.text)
            cloud_metrics_time = time.time() - cloud_info_start
            rest_inf_time = 1 / (resp["End_to_End_pipeline_time"] - start)
            pipeline_time = resp["End_to_End_pipeline_time"] - start
        else: 
            end = time.time()
            rest_inf_time = 1 / (end - start)
            pipeline_time = end - start
        
        frame_count += 1
        if frame_count % 10 == 0:
            logger.info( "--------------------------TIME TAKEN REPORT-------------------------------")
            logger.info("Application-side Preprocess time    : %s", round((preprocess_time_avg / 10),4))
            logger.info("Inference time     : %s", round((inference_time_avg / 10),4))
            logger.info("Application-side Postprocess time   : %s", round((postprocess_time_avg / 10),4))
            frame_count = 0
            frame_read_time_avg = 0
            preprocess_time_avg = 0
            inference_time_avg = 0
            postprocess_time_avg = 0
            normal_cam_read = 0
            web_ui_time_avg = 0

            if args["cloud_address"] != "":
                logger.info("Cloud metrics time : %s",round(
                            (cloud_metrics_time_avg / 10),2))
                cloud_metrics_time_avg = 0
            logger.info("Total time taken   : %s", round((pipeline_time_avg / 10),4))
            logger.info("END_TO_END_FPS     : %s", round((rest_inf_time_avg / 10),4))
            logger.info("Pure inference FPS : %s", round((pure_inf_time_avg / 10),4))
            rest_inf_time_avg = 0
            pure_inf_time_avg = 0
            pipeline_time_avg = 0
            logger.info(
                "-----------------------------END-------------------------------------------")
        else:
            frame_read_time_avg += frame_read_time
            preprocess_time_avg += preprocess_time
            inference_time_avg += inference_time
            postprocess_time_avg += postprocess_time
            pipeline_time_avg += pipeline_time
            if args["cloud_address"] != "":
                cloud_metrics_time_avg += cloud_metrics_time
            rest_inf_time_avg += rest_inf_time
            pure_inf_time_avg += pure_inference_time
            
        frame_num=frame_num+1

def gen_frames():
    """
    Generate video frames with inference results for streaming.
    """
    global arr,model_data, frame_num, right_classes, right_scores, right_boxes, rest_inf_time, pure_inference_time, total_count, pipeline_time, acc, model_data,size
    global pipeline_time_avg, normal_cam_read, frame_read_time_avg, preprocess_time_avg, inference_time_avg, postprocess_time_avg, cloud_metrics_time_avg, frame_count, pure_inf_time_avg, rest_inf_time_avg, web_ui_time, web_ui_fps, web_ui_fps_avg
    global label_name

    #GRPC start
    inference_conn = start_grpc_connection(args["ipaddr"], args["inf_port"])

    if model_type != "classification" :
    
        color_dict = assign_colors_to_class_ids(desired_class_ids)
        label_dict = {label_name[int(class_ids)]: 0 for class_ids in desired_class_ids} 
    else :
        color_dict = None
        label_dict = None
        
    while(camera.isOpened()):
        logger.info(f"--------------------------Frame no. - {frame_num}-------------------------------")
        #Reading Frames
        start = time.time()
        ret, frame = camera.read()  
        normal_cam_read += time.time() - start    
        frame_read_time = camera.frame_read_time
        if frame is None:
            logger.error('End of video/video not detected')
            exit()

        #preprocess
        prepro_start_time = time.time()
        img = cv2.resize(frame, (size, size))        
        preprocess_time = time.time() - prepro_start_time
        
        #sending frames frames for inferencing
        if model_type == "classification" :
        
            global classes_2, classification_scores,arr
            inf_start = time.time()
            right_boxes, right_classes, right_scores, model_load_time, pure_inference_time = send_for_inference(img, inference_conn)
            inference_time = time.time() - inf_start
            postpro_start = time.time()
            classes_2 = classification(label_name,right_classes)
            arr = dict(zip(classes_2,right_scores))
            postprocess_time = time.time() - postpro_start
        else:
            inf_start = time.time()
            right_boxes,right_classes,right_scores,model_load_time, pure_inference_time = send_for_inference(img, inference_conn)
            inference_time = time.time() - inf_start
            #drawing bounding boxes
            postpro_start = time.time()
            arr, img = draw_bb(right_boxes, right_classes, right_scores,frame,color_dict,label_dict,label_name)
            postprocess_time = time.time() - postpro_start
            
        #yield the frames
        if cloud_stream is None:
            image_string = cv2.imencode('.jpg', img)[1]
            image_tensor = image_string.tobytes()
            yield (b'--frame\r\n'b'Content-Type: image/jpeg\r\n\r\n' + image_tensor + b'\r\n')
        else:
            ret, cloud_frame = cloud_stream.read()
            if not(cloud_frame is None):
                cl_image_str = cv2.imencode('.jpg', cloud_frame)[1]
                cl_image_byt = cl_image_str.tobytes()
                yield (b'--frame\r\n'b'Content-Type: image/jpeg\r\n\r\n' + cl_image_byt + b'\r\n')
        web_ui_time = time.time() - start
        web_ui_fps = 1 / web_ui_time
        total_count=sum(arr.values())

        logger.info("Application-side Preprocess time: %s", round(preprocess_time,4))
        logger.info("Application-side Postprocess time: %s", round(postprocess_time,4))
        logger.info("Web UI video FPS: %s", round(web_ui_fps,4))

        if model_type != "classification" and args["cloud_address"] != "" :
        
            cloud_info_start = time.time()
            image_string = cv2.imencode('.jpg', img)[1]
            image_tensor = image_string.tolist()
            
            iot_resp = {"cloud_metrics":{"final_detection": arr,
                            "pure_fps": pure_inference_time, "end_to_end_fps": rest_inf_time, "total_count": total_count}, 
                            "image": image_tensor,
                            "app_time": {"Start_time": 0, "Cloud_start": 0}}
            
            resp = requests.post("http://"+ args["cloud_address"] +"/video",json=iot_resp)
            resp = json.loads(resp.text)
            cloud_metrics_time = time.time() - cloud_info_start
            rest_inf_time = 1 / (resp["End_to_End_pipeline_time"] - start)
            pipeline_time = resp["End_to_End_pipeline_time"] - start
        else: 
            end = time.time()
            rest_inf_time = 1 / (end - start)
            pipeline_time = end - start
        
        frame_count += 1
        if frame_count % 10 == 0:
            logger.info("--------------------------TIME TAKEN REPORT-------------------------------")
            logger.info("Preprocess time    : %s", round((preprocess_time_avg / 10),4))
            logger.info("Inference time     : %s", round((inference_time_avg / 10),4))
            logger.info("Postprocess time   : %s", round((postprocess_time_avg / 10),4))
            logger.info("Web UI frames FPS : %s",  round((web_ui_fps_avg / 10),4))
            frame_count = 0
            frame_read_time_avg = 0
            preprocess_time_avg = 0
            inference_time_avg = 0
            postprocess_time_avg = 0
            normal_cam_read = 0
            web_ui_fps_avg = 0

            if args["cloud_address"] != "":
                logger.info("Cloud metrics time : %s",round((cloud_metrics_time_avg / 10),2))
                cloud_metrics_time_avg = 0
            logger.info("Total time taken   : %s", round((pipeline_time_avg / 10),4))
            logger.info("END_TO_END_FPS     : %s", round((rest_inf_time_avg / 10),4))
            logger.info("Pure inference FPS : %s", round((pure_inf_time_avg / 10),4))
            rest_inf_time_avg = 0
            pure_inf_time_avg = 0
            pipeline_time_avg = 0
            logger.info("-----------------------------END-------------------------------------------")
        else:
            frame_read_time_avg += frame_read_time
            preprocess_time_avg += preprocess_time
            inference_time_avg += inference_time
            postprocess_time_avg += postprocess_time
            pipeline_time_avg += pipeline_time
            if args["cloud_address"] != "" and model_type != "classification" :
                cloud_metrics_time_avg += cloud_metrics_time
            rest_inf_time_avg += rest_inf_time
            pure_inf_time_avg += pure_inference_time
            web_ui_fps_avg += web_ui_fps
        frame_num=frame_num+1

logger.info(f"cloud_service- {args['cloud_service']}")

if args["cloud_address"] == "aws":
    """Sending all the metrics to the WEB UI using flask if cloud is AWS"""
    @app.route('/total_count')
    def total_count():
        def update():
            global total_count
            yield f'data: {total_count}\n\n'
        return flask.Response(update(), mimetype='text/event-stream')

    @app.route('/rest_fps')
    def rest_fps():
        def update():
            global rest_inf_time
            yield f'data: {rest_inf_time:.3f}\n\n'
        return flask.Response(update(), mimetype='text/event-stream')

    @app.route('/pure_fps')
    def pure_fps():
        def update():
            global pure_inference_time
            yield f'data: {pure_inference_time:.3f}\n\n'
        return flask.Response(update(), mimetype='text/event-stream')
    
    @app.route('/web_fps')
    def web_fps():
        def update():
            global web_ui_fps
            yield f'data: {web_ui_fps:.3f}\n\n'
        return flask.Response(update(), mimetype='text/event-stream')

    @app.route('/final_detection')
    def final_detection():
        def update():
            global arr
            yield f'data: {arr}\n\n'
        return flask.Response(update(), mimetype='text/event-stream')

else:
    @app.route('/total_count')
    def total_count():
        def update():
            global total_count
            yield f'data: {total_count}\n\n'
        return flask.Response(update(), mimetype='text/event-stream')
    
    @app.route('/web_fps')
    def web_fps():
        def update():
            global web_ui_fps
            yield f'data: {web_ui_fps:.3f}\n\n'
        return flask.Response(update(), mimetype='text/event-stream')
    
    @app.route('/rest_fps')
    def rest_fps():
        def update():
            global rest_inf_time
            yield f'data: {rest_inf_time:.3f}\n\n'
        return flask.Response(update(), mimetype='text/event-stream')

    @app.route('/pure_fps')
    def pure_fps():
        def update():
            global pure_inference_time
            yield f'data: {pure_inference_time:.3f}\n\n'
        return flask.Response(update(), mimetype='text/event-stream')

    @app.route('/final_detection')
    def final_detection():
        def update():
            global arr
            yield f'data: {arr}\n\n'
        return flask.Response(update(), mimetype='text/event-stream')
    
@app.route('/video_feed')
def video_feed():
    return flask.Response(gen_frames(), mimetype='multipart/x-mixed-replace; boundary=frame')

"""Function to render the HTML page in the WEB Browser.... The user can select the number of devices to be added to the web ui using below code snippets"""
@app.route('/')
def index():
    """Video streaming home page."""
    global model_data
    if args["cloud_address"] != "" and args["cloud_service"]=="alibaba" and args["linkvisual"]=="": 
        logger.info("Alibaba%s")
        # For opening the WEB UI with only one tab for one device
        return render_template('index_alibaba.html',device_1=device_1, link_1=dev_link_1, cloud_service=args["cloud_service"])
    
        # For adding 2nd device uncomment the below return code.
        # Note that the variables for 2rd device should be uncommented in respective html file in line number 84 and 85. 
        # an entry should be created in config/config.json for execution without any errors.
        # return render_template('index_alibaba.html',device_1=device_1, device_2=device_2, link_1=dev_link_1, link_2=dev_link_2,cloud_service=args["cloud_service"])


#webui_stream

    if args["cloud_address"] != "" and args["cloud_service"]=="aws":
        """setting statistics data"""
        if model_type == "classification":
            message="CLASSIFICATION STATISTICS"
        else:
            message="OBJECT DETECTION STATISTICS"

        if args["webui_stream"]=="local":
            """for html through KVS when CSP is disabled - Local stream"""
            logger.info("Using local streaming on web UI%s")
            # For opening the WEB UI with only one tab for one device
            return render_template('index_KVS_local.html', device_1=device_1,link_1=dev_link_1,cloud_service=args["cloud_service"],message=message,acc=acc,model_data=model_infor)

            # For adding 2nd device uncomment the below return code.
            # Note that the variables for 2rd device should be uncommented in respective html file in line number 240 and 241 and an entry should be created in config/config.json for execution without any errors.
            # return render_template('index_KVS_local.html',device_1=device_1, device_2=device_2, link_1=dev_link_1, link_2=dev_link_2,cloud_service=args["cloud_service"],message=message,acc=acc,model_data=model_infor)

        elif args["webui_stream"]=="kvs":
            """for html through KVS stream when CSP is enabled"""
            # For opening the WEB UI with only one tab for one device
            logger.info("Using KVS streaming on web UI.%s")
            return render_template('index_kvs_rtsp.html',device_1=device_1,link_1=dev_link_1,cloud_service=args["cloud_service"],message=message,acc=acc,model_data=model_infor, accesskey=kvs_new_config["accessKey"],secretKey=kvs_new_config["secretKey"],region=kvs_new_config["region"],streamName=kvs_new_config["streamName"])

            # For adding 2nd device uncomment the below return code.
            # Note that the variables for 2rd device should be uncommented in respective html file in line number 241 and 242 and an entry should be created in config/config.json for execution without any errors.
            # return render_template('index_kvs_rtsp.html',device_1=device_1, device_2=device_2, link_1=dev_link_1, link_2=dev_link_2,cloud_service=args["cloud_service"],message=message,acc=acc,model_data=model_infor, accesskey=kvs_new_config["accessKey"],secretKey=kvs_new_config["secretKey"],region=kvs_new_config["region"],streamName=kvs_new_config["streamName"])

    else:
        """setting statistics data"""
        if model_type == "classification" :
            message="CLASSIFICATION STATISTICS"
        else:
            message="OBJECT DETECTION STATISTICS"
        logger.info("Using local streaming on web UI%s")
        # For opening the WEB UI with only one tab for one device
        return render_template('local_stream.html',device_1=device_1,link_1=dev_link_1,cloud_service=args["cloud_service"],message=message,acc=acc,model_data=model_infor)

        # For adding 2nd device uncomment the below return code.
        # Note that the variables for 2rd device should be uncommented in respective html file in line number 235 and 236 and an entry should be created in config/config.json for execution without any errors.
        # return render_template('local_stream.html',device_1=device_1, device_2=device_2, link_1=dev_link_1, link_2=dev_link_2,cloud_service=args["cloud_service"],message=message,acc=acc,model_data=model_infor)


@app.route('/video_server')                                                                                                                           
def video_server():    
    """Redirect to the cloud address specified in args."""                                                                                                                          
    return redirect("http://" + args["cloud_address"])
    
if __name__ == '__main__':
    if args["disable_ui"]:
        app.run(host="0.0.0.0")
    else:
        no_ui_inference()
